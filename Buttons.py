import sys

from PyQt5.QtWidgets import QApplication, QFormLayout, QGridLayout, QLabel, QPushButton, QWidget, QDialog
from PyQt5.QtGui import QImage, QPalette, QBrush, QFont, QFontDatabase, QIcon, QPixmap
from PyQt5.QtCore import QSize, Qt, QEvent

from Game import Game
import pygame

class StartButton(QPushButton):

    def __init__(self, window, parent=None, dropDown_level=None):
        super(StartButton, self).__init__(parent)
        self.hoverSound = pygame.mixer.Sound("sound/button_hover.mp3")
        self.clickSound = pygame.mixer.Sound("sound/button_click.mp3")
        self.clickSoundRelease = pygame.mixer.Sound("sound/button_click_released.mp3")
        self.dropDown_level = dropDown_level
        self.setVolumeAllSounds(0.05)
        self.window = window
        # other initializations...

    
    def setVolumeAllSounds(self, x):
        self.hoverSound.set_volume(x)
        self.clickSound.set_volume(x)
        self.clickSoundRelease.set_volume(x)

    def enterEvent(self, QEvent):
        if QEvent.type() == QEvent.Enter:
            pygame.mixer.Sound.play(self.hoverSound)
        pass

    def leaveEvent(self, QEvent):
        # here the code for mouse leave
        pass

    def mousePressEvent(self, QKeyEvent):
        pygame.mixer.Sound.play(self.clickSound)
    
    
    def mouseReleaseEvent(self, QKeyEvent):
        self.clickSoundRelease.play()
        pygame.mixer.music.stop()
        self.window.hide()
        game = Game(self.dropDown_level.currentIndex(), self.window)

class CreditButton(QPushButton):

    def __init__(self, window, parent=None):
        super(CreditButton, self).__init__(parent)
        self.hoverSound = pygame.mixer.Sound("sound/button_hover.mp3")
        self.clickSound = pygame.mixer.Sound("sound/button_click.mp3")
        self.clickSoundRelease = pygame.mixer.Sound("sound/button_click_released.mp3")
        
        self.setVolumeAllSounds(0.05)
        self.window = window
        # other initializations...
    
    def setVolumeAllSounds(self, x):
        self.hoverSound.set_volume(x)
        self.clickSound.set_volume(x)
        self.clickSoundRelease.set_volume(x)

    def enterEvent(self, QEvent):
        if QEvent.type() == QEvent.Enter:
            pygame.mixer.Sound.play(self.hoverSound)
        pass

    def leaveEvent(self, QEvent):
        # here the code for mouse leave
        pass

    def mousePressEvent(self, QKeyEvent):
        pygame.mixer.Sound.play(self.clickSound)
    
    
    def mouseReleaseEvent(self, QKeyEvent):
        self.clickSoundRelease.play()
        dlg = QDialog(self)
        dlg.setWindowTitle("Credits")
        dlg.width = 500
        dlg.height = 500
        info = QLabel("Sound von: https://www.zapsplat.com/")
        layout = QGridLayout()
        layout.addWidget(info, 0, 0, 1, 1)
        dlg.setLayout(layout)
        dlg.exec()

class LeaveButton(QPushButton):

    def __init__(self, window, parent=None):
        super(LeaveButton, self).__init__(parent)
        self.hoverSound = pygame.mixer.Sound("sound/button_hover.mp3")
        self.clickSound = pygame.mixer.Sound("sound/button_click.mp3")
        self.clickSoundRelease = pygame.mixer.Sound("sound/button_click_released.mp3")
        
        self.setVolumeAllSounds(0.05)
        self.window = window
        # other initializations...
    
    def setVolumeAllSounds(self, x):
        self.hoverSound.set_volume(x)
        self.clickSound.set_volume(x)
        self.clickSoundRelease.set_volume(x)

    def enterEvent(self, QEvent):
        if QEvent.type() == QEvent.Enter:
            pygame.mixer.Sound.play(self.hoverSound)
        pass

    def leaveEvent(self, QEvent):
        # here the code for mouse leave
        pass

    def mousePressEvent(self, QKeyEvent):
        pygame.mixer.Sound.play(self.clickSound)
    
    
    def mouseReleaseEvent(self, QKeyEvent):
        self.clickSoundRelease.play()
        pygame.mixer.music.stop()
        self.window.close()