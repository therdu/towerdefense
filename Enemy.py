import pygame, math

class Enemy:
    def __init__(self, image, health, money_value, waypoints, mul, image_enemy1_hit, image_enemy1, steps):
        self.steps = steps 
        self.mult = mul
        self.image = pygame.transform.scale(image, (100, 100))
        self.health = health
        self.money_value = money_value
        self.pointerPosition = 0
        self.cooldown = 0
        self.image_enemy1_hit = image_enemy1_hit[1]
        self.image_normal = image_enemy1[1]
        self.images_normal = image_enemy1
        self.images_hit = image_enemy1_hit
        self.movement = 0
        self.maxHealth = health
        
        if not waypoints == None:
            self.position = waypoints[0]         
            self.waypoints = self.calcWaypointsWithSteps(waypoints)
            
        

    def updateCooldown(self):
        if not self.cooldown == 0:
            self.cooldown -= 1
            if self.cooldown == 0:
                self.image = self.image_normal
    
    def getsHit(self, damage):
        # update values after hit
        self.health -= damage
        self.cooldown = 10
        #change image
        self.image = self.image_enemy1_hit
    
    def calcWaypointsWithSteps(self, waypoints):
        curPos = waypoints[0]
        waypointsWithSteps = [(curPos[0]*self.mult[0], curPos[1]*self.mult[1]-20)]

        for point in waypoints[1:]:
            # diff to next point
            diffX = point[0] - curPos[0]
            diffY = point[1] - curPos[1]
            for i in range(self.steps):
                # get points inbetween two waypoints
                xPos = curPos[0] + diffX / self.steps
                yPos = curPos[1] + diffY / self.steps
                waypointsWithSteps.append((xPos*self.mult[0], yPos*self.mult[1]-20))
                curPos = (xPos, yPos)

        return waypointsWithSteps

    def getNewPosition(self):
        self.movement += 0.1
        if self.movement == 360:
            self.movement = 0
        self.pointerPosition += 1
        # if out of image -> return None
        if(self.pointerPosition > len(self.waypoints)-1):
            return None
        if self.position[0] < self.waypoints[self.pointerPosition][0]:
            if self.cooldown == 0:
                self.image = self.images_normal[1]
            self.image_normal = self.images_normal[1]
            self.image_enemy1_hit = self.images_hit[1]
        elif self.position[0] > self.waypoints[self.pointerPosition][0]: 
            if self.cooldown == 0:
                self.image = self.images_normal[0]
            self.image_normal = self.images_normal[0]
            self.image_enemy1_hit = self.images_hit[0]
        # otherwise move pointer and return new position
        self.position = self.waypoints[self.pointerPosition]
        return [self.position[0] + + 3*math.sin(self.movement), self.position[1] + 3*math.sin(self.movement)]