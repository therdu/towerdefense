import pygame

class InputBox:

    def __init__(self, x, y, w, h, font, text=''):
        self.rect = pygame.Rect(x, y, w, h)
        self.color_inactive = (125, 125, 125)
        self.color_active = (200, 200, 200)
        self.color = self.color_inactive
        self.color_background = (255, 255, 255)
    
        self.text = text
        self.font = font
        self.txt_surface = self.font.render(self.text, True, (0, 0, 0))
        self.active = False

    def change_color(self, color_active, color_inactive, color_background):
        self.color_active = color_active
        self.color_inactive = color_inactive
        self.color_background = color_background
        
        if not self.active:
            self.color = self.color_inactive
        else:
            self.color = self.color_active
    
    def setFont(self, font):
        self.font = font
    
    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            # If the user clicked on the input_box rect.
            if self.rect.collidepoint(event.pos):
                # Toggle the active variable.
                self.active = not self.active
            else:
                self.active = False
            # Change the current color of the input box.
            self.color = self.color_active if self.active else self.color_inactive
        if event.type == pygame.KEYDOWN:
            if self.active:
                if event.key == pygame.K_RETURN:
                    self.active = False
                    self.color = self.color_inactive
                elif event.key == pygame.K_BACKSPACE:
                    self.text = self.text[:-1]
                else:
                    # if not text too long, add it
                    if self.font.render((self.text + event.unicode), True, (0, 0, 0)).get_rect().width <= self.rect.width - 5:
                        self.text += event.unicode
                # Re-render the text.
                self.txt_surface = self.font.render(self.text, True, (0, 0, 0))
                return True
            else:
                return False

    def update(self):
        # Resize the box if the text is too long
        width = max(200, self.txt_surface.get_width()+10)
        self.rect.w = width

    def draw(self, screen):
        # Blit the background
        pygame.draw.rect(screen, self.color_background, self.rect, 0)
        # Blit the text
        screen.blit(self.txt_surface, (self.rect.x+5, self.rect.y+5))
        # Blit the rect
        pygame.draw.rect(screen, self.color, self.rect, 2)


