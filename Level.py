import numpy as np
import random
from Enemy import Enemy

class Level:
    
    def __init__(self, waypoints, enemies, endless, startMoney, p, changeP, mul):
        self.waypoints = waypoints # lists of all waypoints
        self.enemies = enemies # if random = True: contains types of enemies, else contains specific enemies
        self.endless = endless # if level is endless or not
        self.enemyIndex = 0 # enemy counter 
        self.startMoney = startMoney # start money for the player
        self.p = p # probability of spawning a monster
        self.initP = p
        self.changeP = changeP # does the probability change? 0 = no,  bigger 0 = getting more, lower 0 = getting less
        self.mul = mul
        
        self.lastEnemy = 0 # counts how long no enemy was spawn
        
    def getNextEnemy(self):
        # probability of spawning an enemy
        self.p = self.p+(2**(-abs(self.changeP))*0.1)*np.sign(self.changeP)
        if self.p < 0.3:
            self.p = 0.3
        if self.p > 1:
            self.p = 1
        randomNum = random.random()
        if randomNum < self.p or self.lastEnemy > 10:
            # if not endless-mode
            if not self.endless:
                # and enemies left
                if len(self.enemies) > self.enemyIndex:     
                    # return enemy
                    enemy = self.enemies[self.enemyIndex]
                    self.enemyIndex += 1
                    return enemy
                else:
                    return None
                
            # endless mode
            else:
                enemy = None
                
                # calculate, which enemy should spawn
                maxEnemies = len(self.enemies)
                counter = 0
                chooseEnemy = 1 / ((maxEnemies + counter)**abs(self.p - self.initP))
                
                if random.random() > chooseEnemy:
                    counter += 1
                    
                enemyTypeIndex = abs(int((random.random()-0.000001) / (1/maxEnemies-counter)))
                enemyType = self.enemies[enemyTypeIndex]
                
                # calculate the health of the enemy
                enemy_health = 70
                if random.random() < (self.p + abs(self.initP - self.p)) - 0.1:
                    enemy_health = enemyType.health * (1+(0.1*self.enemyIndex))
                
                self.enemy_value = enemyType.money_value + int(enemyType.money_value * (self.enemyIndex/16))

                waypointsIndex = abs(int((random.random()-0.000001) / (1/len(self.waypoints)))) 
                enemy = Enemy(enemyType.image, enemy_health, self.enemy_value, self.waypoints[waypointsIndex], self.mul,
                              enemyType.images_hit, enemyType.images_normal, enemyType.steps)

        
                self.enemyIndex += 1
                return enemy
        else: 
            self.lastEnemy += 1
            return None
    
                