from Enemy import Enemy
from Level import Level
import pygame, random

class LevelSelection:
    
    def __init__(self, scale_images):
        self.level = []
        self.scale_images = scale_images
        
        # creating the ways
        waypoints1 = [(0, 1, 5), (1, 1, 5), (2, 1, 2), (2, 2, 10), (2, 3, 11),(3,3,5),(4,3,2),(4,4,11),(5,4,5),(6,4,5),(7,4,2),(7,5,10),(7,6,11),(8,6,2),(8,7,10),(8,8,3),(7,8,5),(6,8,4),(6,9,3),(5,9,5),(4,9,5),(3,9,11),(3,8,10),(3,7,2),(2,7,5),(1,7,5),(0,7,5)]
        waypoints2 = [(0,9,5),(1,9,5),(2,9,3),(2,8,10),(2,7,10),(2,6,4),(3,6,5),(4,6,3),(4,5,4),(5,5,5),(6,5,3),(6,4,10),(6,3,10),(6,2,10),(6,1,10),(6,0,10)]
        waypoints3 = [(4,9,10),(4,8,10),(4,7,10),(4,6,4),(5,6,5),(6,6,5),(7,6,5),(8,6,3),(8,5,10),(8,4,10),(8,3,10),(8,2,10),(8,1,2),(7,1,5),(6,1,5),(5,1,5),(4,1,5),(3,1,5),(2,1,5),(1,1,4),(1,2,10),(1,3,10),(1,4,3),(0,4,5)]

        waypoints4_1 = [(3,0,10 ),(3,1,10), (3,2,9), (4,2,5), (5,2,5), (6,2,2), (6,3,10), (6,4,10), (6,5,10), (6,6,3), (5,6,5), (4,6,5), (3,6,5), (2,6,1), (1,6,5), (0,6,5)]
        waypoints4_2 = [(3,0,10), (3,1,10), (3,2,9), (2,2,4), (2,3,10), (2,4,10), (2,5,10), (2,6,1), (2,7,10), (2,8,11), (3,8,5), (4,8,5), (5,8,5), (6,8,5), (7,8,2), (7,9,10)]
        waypoints5_1 = [(1, 9, 10), (1, 8, 10), (1, 7, 10), (1, 6, 10), (1, 5, 4), (2, 5, 5), (3, 5, 5), (4, 5, 5),(5, 5, 9), (5, 4, 10), (5, 3, 10), (5, 2, 10), (5, 1, 10), (5, 0, 10)]
        waypoints5_2 = [(1, 9, 10), (1, 8, 10), (1, 7, 10), (1, 6, 10), (1, 5, 4), (2, 5, 5), (3, 5, 5), (4, 5, 5),(5, 5, 9), (6, 5, 5), (7, 5, 5), (8, 5, 2), (8, 6, 10), (8, 7, 10), (8, 8, 10), (8, 9, 10)]

        waypoints6_1 = [(0,7,3),(0,6,10),(0,5,4),(1,5,5),(2,5,5),(3,5,1),(4,5,5),(5,5,5),(6,5,5),(7,5,3),(7,4,10),(7,3,10),(7,2,2),(6,2,5),(5,2,5),(4,2,9),(4,1,10),(4,0,10)]
        waypoints6_2 = [(5,9,10),(5,8,10),(5,7,2),(4,7,5),(3,7,11),(3,6,10),(3,5,1),(3,4,10),(3,3,10),(3,2,4),(4,2,9),(4,1,10),(4,0,10)]
        waypoints7_1 = [(0, 0, 5), (1, 0, 2), (1, 1, 10), (1, 2, 10), (1, 3, 11), (2, 3, 2), (2, 4, 11), (3, 4, 5),(4, 4, 1), (5, 4, 5), (6, 4, 2), (6, 5, 10), (6, 6, 11), (7, 6, 2), (7, 7, 11), (8, 7, 5),(9, 7, 2), (9, 8, 10), (9, 9, 10)]
        waypoints7_2 = [(9, 0, 10), (9, 1, 3), (8, 1, 5), (7, 1, 5), (6, 1, 4), (6, 2, 3), (5, 2, 5), (4, 2, 4),(4, 3, 10), (4, 4, 1), (4, 5, 10), (4, 6, 10), (4, 7, 10), (4, 8, 3), (3, 8, 5), (2, 8, 4),(2, 9, 10)]

        waypoints8_1 = [(2,0,10),(2,1,1),(2,2,10),(2,3,10),(2,4,10),(2,5,11),(3,5,5),(4,5,5),(5,5,1),(6,5,5),(7,5,2),(7,6,10),(7,7,10),(7,8,10),(7,9,10)]
        waypoints8_2 = [(0,1,5),(1,1,5),(2,1,1),(3,1,5),(4,1,5),(5,1,5),(6,1,5),(7,1,2),(7,2,10),(7,3,3),(6,3,5),(5,3,4),(5,4,10),(5,5,1),(5,6,10),(5,7,10),(5,8,3),(4,8,5),(3,8,5),(2,8,5),(1,8,4),(1,9,10)]
        waypoints9_1 = [(0,1,5),(1,1,2),(1,2,10),(1,3,10),(1,4,10),(1,5,11),(2,5,5),(3,5,1),(4,5,5),(5,5,5),(6,5,5),(7,5,5),(8,5,2),(8,6,10),(8,7,10),(8,8,10),(8,9,10)]
        waypoints9_2 = [(0,9,4),(1,9,5),(2,9,5),(3,9,3),(3,8,10),(3,7,10),(3,6,10),(3,5,1),(3,4,10),(3,3,4),(4,3,5),(5,3,3),(5,2,10),(5,1,10),(5,0,10)]
        waypoints10 = [(4,0,10),(4,1,10),(4,2,10),(4,3,10),(4,4,10),(4,5,10),(4,6,10),(4,7,10),(4,8,10),(4,9,10)]
        waypoints11_1 = [(2,0,10),(2,1,10),(2,2,10),(2,3,10),(2,4,10),(2,5,10),(2,6,10),(2,7,10),(2,8,10),(2,9,10)]
        waypoints11_2 = [(7,9,10),(7,8,10),(7,7,10),(7,6,10),(7,5,10),(7,4,10),(7,3,10),(7,2,10),(7,1,10),(7,0,10)]
        waypoints12_1 = [(0,4,5),(1,4,5),(2,4,5),(3,4,5),(4,4,5),(5,4,7),(5,3,10),(5,2,10),(5,1,10),(5,0,10)]
        waypoints12_2 = [(0,4,5), (1,4,5), (2,4,5), (3,4,5), (4,4,5), (5,4,7), (5,5,10), (5,6,10), (5,7,10), (5,8,10), (5,9,10)]
        # waypoints13 einfach (Spirale)
        waypoints13 = [(0,1,5), (1,1,5), (2,1,5), (3,1,5), (4,1,5), (5,1,5), (6,1,5), (7,1,5), (8,1,2), (8,2,10), (8,3,10), (8,4,10), (8,5,10), (8,6,10), (8,7,10), (8,8,3), (7,8,5), (6,8,5), (5,8,5), (4,8,5), (3,8,5), (2,8,5),(1,8,11),(1,7,10),(1,6,4),(2,6,5),(3,6,5),(4,6,5),(5,6,5),(6,6,3),(6,5,10),(6,4,10),(6,3,2),(5,3,5),(4,3,5),(3,3,5),(2,3,5),(1,3,5),(0,3,5)]
        waypoints14_1 = [(0,4,5), (1,4,5), (2,4,5), (3,4,5), (4,4,5), (5,4,3), (5,3,10), (5,2,10), (5,1,10), (5,0,10)]
        waypoints14_2 = [(0,6,5), (1,6,5), (2,6,5), (3,6,5), (4,6,5), (5,6,2), (5,7,10), (5,8,10), (5,9,10)]
        waypoints14_3 = [(7,0,10), (7,1,10), (7,2,10), (7,3,10), (7,4,10), (7,5,10), (7,6,10), (7,7,10), (7,8,10), (7,9,10) ]
        waypoints15_1 = [(4,0,10), (4,1,10), (4,2,10), (4,3,3), (3,3,4), (3,4,10), (3,5,11), (4,5,2), (4,6,10), (4,7,10), (4,8,10), (4,9,10)]
        waypoints15_2 = [(5,0,10), (5,1,10), (5,2,10), (5,3,11), (6,3,2), (6,4,10), (6,5,3), (5,5,4), (5,6,10), (5,7,10), (5,8,10), (5,9,10)]
        self.ways = {0: [waypoints1], 1: [waypoints2], 2: [waypoints3], 3: [waypoints4_1, waypoints4_2], 4: [waypoints5_1, waypoints5_2], 5: [waypoints6_1, waypoints6_2], 6: [waypoints7_1, waypoints7_2], 7: [waypoints8_1, waypoints8_2], 8: [waypoints9_1, waypoints9_2], 9: [waypoints10], 10: [waypoints11_1, waypoints11_2], 11: [waypoints12_1, waypoints12_2], 12:[waypoints13], 13:[waypoints14_1,waypoints14_2,waypoints14_3], 14:[waypoints15_1, waypoints15_2]}
        
        # enemy images
        self.image_enemy1 = pygame.image.load("images/enemies/enemy1.png")
        self.image_enemy1 = pygame.transform.scale(self.image_enemy1, self.scale_images)
        self.image_enemy1_hit = pygame.image.load("images/enemies/enemy1_hit.png")
        self.image_enemy1_hit = pygame.transform.scale(self.image_enemy1_hit, self.scale_images)
        
        self.image_enemy1_right = pygame.image.load("images/enemies/enemy1_right.png")
        self.image_enemy1_right = pygame.transform.scale(self.image_enemy1_right, self.scale_images)
        self.image_enemy1_right_hit = pygame.image.load("images/enemies/enemy1_right_hit.png")
        self.image_enemy1_right_hit = pygame.transform.scale(self.image_enemy1_right_hit, self.scale_images)
        
        self.image_enemy2 = pygame.image.load("images/enemies/enemy2.png")
        self.image_enemy2 = pygame.transform.scale(self.image_enemy2, self.scale_images)
        self.image_enemy2_hit = pygame.image.load("images/enemies/enemy2_hit.png")
        self.image_enemy2_hit = pygame.transform.scale(self.image_enemy2_hit, self.scale_images)
        
        self.image_enemy2_right = pygame.image.load("images/enemies/enemy2_right.png")
        self.image_enemy2_right = pygame.transform.scale(self.image_enemy2_right, self.scale_images)
        self.image_enemy2_right_hit = pygame.image.load("images/enemies/enemy2_right_hit.png")
        self.image_enemy2_right_hit = pygame.transform.scale(self.image_enemy2_right_hit, self.scale_images)
        
        self.image_enemy3 = pygame.image.load("images/enemies/enemy3.png")
        self.image_enemy3 = pygame.transform.scale(self.image_enemy3, self.scale_images)
        self.image_enemy3_hit = pygame.image.load("images/enemies/enemy3_hit.png")
        self.image_enemy3_hit = pygame.transform.scale(self.image_enemy3_hit, self.scale_images)
        
        self.image_enemy3_right = pygame.image.load("images/enemies/enemy3_right.png")
        self.image_enemy3_right = pygame.transform.scale(self.image_enemy3_right, self.scale_images)
        self.image_enemy3_right_hit = pygame.image.load("images/enemies/enemy3_right_hit.png")
        self.image_enemy3_right_hit = pygame.transform.scale(self.image_enemy3_right_hit, self.scale_images)
        
        
        # creating the enemies     
        # enemy values
        self.enemy1_value = 3
        self.enemy2_value = 1
        self.enemy3_value = 2
        
        self.resetLevel()
    # return a Level
    def getLevel(self, index, endless):
        return self.level[endless][index]
    
    def resetLevel(self):
        # creating the enemies     
        # enemy values
        self.level = []

        enemies1 = self.getEnemyList_simple(0, 0, 30, 0, 1.07)
        enemies2 = self.getEnemyList_simple(0, 0, 50, 1, 1.07)
        enemies3 = self.getEnemyList_pause(0, 10, 40, 2, 1, 1.07)
        enemies4 = self.getEnemyList_simple(0, 20, 40, 3, 1.07)
        enemies5 = self.getEnemyList_simple(0, 20, 15, 4, 1.06)
        enemies6 = self.getEnemyList_simple(0, 10, 25, 5, 1.067)
        enemies7 = self.getEnemyList_simple(5, 5, 25, 6, 1.07)
        enemies8 = self.getEnemyList_simple(5, 20, 40, 7, 1.06)
        enemies9 = self.getEnemyList_simple(10, 30, 0, 8, 1.075)
        enemies10 = self.getEnemyList_simple(20, 30, 40, 9, 1.06)

        enemies11 = self.getEnemyList_simple(20, 30, 10, 10, 1.06)
        enemies12 = self.getEnemyList_simple(20, 40, 00, 11, 1.10)
        enemies13 = self.getEnemyList_simple(30, 00, 00, 12, 1.09)
        enemies14 = self.getEnemyList_simple(30, 20, 40, 13, 1.06)
        enemies15 = self.getEnemyList_simple(50, 30, 40, 14, 1.06)

        # creating the level
        level1 = Level(self.ways.get(0), enemies1, False, 35, 1, 0, self.scale_images)
        level2 = Level(self.ways.get(1), enemies2, False, 35, 1, 0, self.scale_images)
        level3 = Level(self.ways.get(2), enemies3, False, 35, 1, 0, self.scale_images)
        level4 = Level(self.ways.get(3), enemies4, False, 35, 1, 0, self.scale_images)
        level5 = Level(self.ways.get(4), enemies5, False, 35, 1, 0, self.scale_images)
        level6 = Level(self.ways.get(5), enemies6, False, 35, 1, 0, self.scale_images)
        level7 = Level(self.ways.get(6), enemies7, False, 35, 1, 0, self.scale_images)
        level8 = Level(self.ways.get(7), enemies8, False, 35, 1, 0, self.scale_images)
        level9 = Level(self.ways.get(8), enemies9, False, 35, 1, 0, self.scale_images)
        level10 = Level(self.ways.get(9), enemies10, False, 35, 1, 0, self.scale_images)
        level11 = Level(self.ways.get(10), enemies11, False, 50, 1, 0, self.scale_images)
        level12 = Level(self.ways.get(11), enemies12, False, 35, 1, 0, self.scale_images)
        level13 = Level(self.ways.get(12), enemies13, False, 50, 1, 0, self.scale_images)
        level14 = Level(self.ways.get(13), enemies14, False, 80, 1, 0, self.scale_images)
        level15 = Level(self.ways.get(14), enemies15, False, 50, 1, 0, self.scale_images)

        level1Endless = Level(self.ways.get(0), self.getAllTypesOfEnemies(), True, 35, 0.9, -2.5, self.scale_images)
        level2Endless = Level(self.ways.get(1), self.getAllTypesOfEnemies(), True, 35, 0.9, -3, self.scale_images)
        level3Endless = Level(self.ways.get(2), self.getAllTypesOfEnemies(), True, 35, 0.9, 5, self.scale_images)
        level4Endless = Level(self.ways.get(3), self.getAllTypesOfEnemies(), True, 35, 0.9, -2.5, self.scale_images)
        level5Endless = Level(self.ways.get(4), self.getAllTypesOfEnemies(), True, 35, 0.9, -3, self.scale_images)
        level6Endless = Level(self.ways.get(5), self.getAllTypesOfEnemies(), True, 35, 0.9, 5, self.scale_images)
        level7Endless = Level(self.ways.get(6), self.getAllTypesOfEnemies(), True, 35, 0.9, -2.5, self.scale_images)
        level8Endless = Level(self.ways.get(7), self.getAllTypesOfEnemies(), True, 35, 0.9, -3, self.scale_images)
        level9Endless = Level(self.ways.get(8), self.getAllTypesOfEnemies(), True, 35, 0.9, 5, self.scale_images)
        level10Endless = Level(self.ways.get(9), self.getAllTypesOfEnemies(), True, 35, 0.9, 5, self.scale_images)
        level11Endless = Level(self.ways.get(10), self.getAllTypesOfEnemies(), True, 35, 0.9, 5, self.scale_images)
        level12Endless = Level(self.ways.get(11), self.getAllTypesOfEnemies(), True, 35, 0.9, 5, self.scale_images)
        level13Endless = Level(self.ways.get(12), self.getAllTypesOfEnemies(), True, 50, 0.9, 5, self.scale_images)
        level14Endless = Level(self.ways.get(13), self.getAllTypesOfEnemies(), True, 80, 0.9, 5, self.scale_images)
        level15Endless = Level(self.ways.get(14), self.getAllTypesOfEnemies(), True, 50, 0.9, 5, self.scale_images)
        
        level_normal = [level1, level2, level3, level4, level5, level6, level7, level8, level9, level10, level11, level12, level13, level14, level15]
        level_endless = [level1Endless, level2Endless, level3Endless, level4Endless, level5Endless, level6Endless, level7Endless, level8Endless, level9Endless, level10Endless, level11Endless, level12Endless, level13Endless, level14Endless, level15Endless]
        
        self.level.append(level_normal)
        self.level.append(level_endless)
        
        
    
    # create a simple List of enemies
    def getEnemyList_simple(self, numEnemy1, numEnemy2, numEnemy3, way, p):
        enemies = []
        for i in range(0, numEnemy3):
            enemy_value = self.enemy3_value + int(self.enemy3_value * (i/8))
            enemy_health = min(1000, 75*(p)**(i))
            enemies.append(Enemy(self.image_enemy3, 75*(p)**(i), enemy_value, self.getWayForEnemy(way), self.scale_images, [self.image_enemy3_hit, self.image_enemy3_right_hit], [self.image_enemy3, self.image_enemy3_right], 80))
        
        for i in range(0, numEnemy2):
            enemy_value = self.enemy2_value + int(self.enemy2_value * (i+numEnemy3/8))
            enemy_health = min(2000, 50*(p)**(i+(6/5*(numEnemy3))))
            enemies.append(Enemy(self.image_enemy2, enemy_health, enemy_value, self.getWayForEnemy(way), self.scale_images, [self.image_enemy2_hit, self.image_enemy2_right_hit], [self.image_enemy2, self.image_enemy2_right], 40))
        
        for i in range(0, numEnemy1):
            enemy_value = self.enemy1_value + int(self.enemy1_value * ((i+numEnemy3+numEnemy2)/8))
            enemy_health = min(5000, 150*(p)**(i+(8/7*(numEnemy3+numEnemy2))))
            enemies.append(Enemy(self.image_enemy1, enemy_health, enemy_value, self.getWayForEnemy(way), self.scale_images, [self.image_enemy1_hit, self.image_enemy1_right_hit], [self.image_enemy1, self.image_enemy1_right], 100))
        
        return enemies
    
    def getEnemyList_pause(self, numEnemy1, numEnemy2, numEnemy3, way, pause, p):
        enemies = self.getEnemyList_simple(0, 0, numEnemy3, way, p)
        for i in range(0, pause):
            enemies.append(None)
             
        for i in range(0, numEnemy2):
            enemy_value = self.enemy2_value + int(self.enemy2_value * ((i+numEnemy3)/8))
            enemies.append(Enemy(self.image_enemy2, 50*(p)**(i+(6/5*numEnemy3)), enemy_value, self.getWayForEnemy(way), self.scale_images, [self.image_enemy2_hit, self.image_enemy2_right_hit], [self.image_enemy2, self.image_enemy2_right], 40))
        
        for i in range(0, pause):
            enemies.append(None)   
            
        for i in range(0, numEnemy1):
            enemy_value = self.enemy3_value + int(self.enemy3_value * (i+numEnemy3+numEnemy2/8))
            enemies.append(Enemy(self.image_enemy1, 75*(p)**(i+(6/5*(numEnemy3+numEnemy2))), enemy_value, self.getWayForEnemy(way), self.scale_images, [self.image_enemy1_hit, self.image_enemy1_right_hit], [self.image_enemy1, self.image_enemy1_right], 80))
        
        return enemies
       

        
            
     
    # return all different current types of enemies
    def getAllTypesOfEnemies(self):
        enemies = []
        enemies.append(Enemy(self.image_enemy1, 100, self.enemy1_value, None, self.scale_images, [self.image_enemy1_hit, self.image_enemy1_right_hit], [self.image_enemy1, self.image_enemy1_right], 100))
        enemies.append(Enemy(self.image_enemy2, 50, self.enemy2_value, None, self.scale_images, [self.image_enemy2_hit, self.image_enemy2_right_hit], [self.image_enemy2, self.image_enemy2_right], 40))
        enemies.append(Enemy(self.image_enemy3, 75, self.enemy3_value, None, self.scale_images, [self.image_enemy3_hit, self.image_enemy3_right_hit], [self.image_enemy3, self.image_enemy3_right], 80))
        
        return enemies
    
    # get a random way of a level for an enemy
    def getWayForEnemy(self, way):
        waypointsIndex = abs(int((random.random()-0.000001) / (1/len(self.ways.get(way)))))
        return self.ways.get(way)[waypointsIndex]