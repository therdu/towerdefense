import pygame
# init font
pygame.font.init()
class Button:

    def __init__(self, x, y, w, h, font = pygame.font.Font(None, 18), text = " ", fontColor = [(0, 0, 0), (0, 0, 0), (0, 0, 0)], color = [(125, 125, 125), (125, 125, 125), (200, 200, 200)], colorBorder = [(0, 0, 0),(255, 255, 255),(255, 255, 255)]):
        
        self.hoverSound = pygame.mixer.Sound("sound/button_hover.mp3")
        self.clickSound = pygame.mixer.Sound("sound/button_click.mp3")
        self.clickSoundRelease = pygame.mixer.Sound("sound/button_click_released.mp3")
        
        self.clicked = False
        self.click = False
        
        self.rect = pygame.Rect(x, y, w, h)
        self.color_inactive = color[0]
        self.color_hover = color[1]
        self.color_active = color[2]

        self.color = self.color_inactive
        
        self.fontColor_inactive = fontColor[0]
        self.fontColor_hover = fontColor[1]
        self.fontColor_active = fontColor[2]
        
        self.fontColor = self.fontColor_inactive
        
        self.colorBorder_inactive = colorBorder[0]
        self.colorBorder_hover = colorBorder[1]
        self.colorBorder_active = colorBorder[2]
        
        self.colorBorder = self.colorBorder_inactive
        self.font = font
        self.text = text
        self.txt_surface = self.font.render(self.text, True, self.fontColor)
        self.active = False
        
        self.choosed = False
        self.muted = False
        self.setVolume(0.2)
        self.disabled = False
        
        self.disabled_color = (100, 100, 100)
        self.disabled_fontColor = (255, 255, 255)
        
        if self.txt_surface.get_width()+20 > self.rect.w:
            self.rect.w = self.txt_surface.get_width()+20 # Resize the box if the text is too long
        if self.txt_surface.get_height()+20 > self.rect.h:
            self.rect.h = self.txt_surface.get_height()+20
        

    
    def handle_event(self, event):
        # check if hover
        if self.disabled:
            self.color = self.disabled_color
            self.fontColor = self.disabled_fontColor
        else:
            if event.type == pygame.MOUSEMOTION:
                x, y = pygame.mouse.get_pos()
                if x >= self.rect.x and x <= self.rect.x + self.rect.w and y >= self.rect.y and y <= self.rect.y + self.rect.h:
                    if not self.active:
                        if not self.muted:
                            pygame.mixer.Sound.play(self.hoverSound)
                        self.color = self.color_hover
                        self.fontColor = self.fontColor_hover
                        self.colorBorder = self.colorBorder_hover
                        self.active = True
                elif not self.choosed:
                    self.active = False
                    self.color = self.color_inactive
                    self.fontColor = self.fontColor_inactive
                    self.colorBorder = self.colorBorder_inactive
                    
                # check if start clicking
            left, middle, right = pygame.mouse.get_pressed()
            if left:
                x, y = pygame.mouse.get_pos()
                if x >= self.rect.x and x <= self.rect.x + self.rect.w and y >= self.rect.y and y <= self.rect.y + self.rect.h:
                    if not self.click:
                        if not self.muted:
                            pygame.mixer.Sound.play(self.clickSound)
                        self.color = self.color_active
                        self.fontColor = self.fontColor_active
                        self.colorBorder = self.colorBorder_active
                        self.click = True
                    
            elif self.click:
                if not self.muted:
                    pygame.mixer.Sound.play(self.clickSoundRelease)
                self.color = self.color_inactive
                self.fontColor = self.fontColor_inactive
                self.colorBorder = self.colorBorder_inactive
                self.clicked = True
                self.click = False
            else:
                self.clicked = False

            
        
        
    def draw(self, screen):
        # Blit the rect
        pygame.draw.rect(screen, self.color, self.rect, 0, border_radius = 5)
        # Blit the text
        self.txt_surface = self.font.render(self.text, True, self.fontColor)
        screen.blit(self.txt_surface, (self.rect.x + self.rect.w/2 - self.txt_surface.get_width()/2, self.rect.y + self.rect.h/2 - self.txt_surface.get_height()/2))
        # Blit the border
        # pygame.draw.rect(screen, self.colorBorder, self.rect, 1)
        
        
    def setVolume(self, x):
        self.hoverSound.set_volume(x)
        self.clickSound.set_volume(x)
        self.clickSoundRelease.set_volume(x)