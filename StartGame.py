from pygame import *
import pygame

from Tower import Tower
from Enemy import Enemy
from Inputbox import InputBox
from LevelSelection import LevelSelection

from cryptography.fernet import Fernet

import sys, random, math, time, copy

class Game:

    def __init__(self, screen, tile_y_scale, tile_x_scale, num_tiles_x, num_tiles_y):
        super().__init__()
        self.readHighscorelist()
        
        self.screen = screen

        # size of field
        self.tile_y_scale = tile_y_scale
        self.tile_x_scale = tile_x_scale
        
        # window size
        self.x_scale = self.tile_x_scale+210/640*self.tile_y_scale
        
        # num of tiles
        self.num_tiles_x = num_tiles_x
        self.num_tiles_y = num_tiles_y
        
        # scale values
        self.scale_images = (self.tile_x_scale/self.num_tiles_x, self.tile_y_scale/self.num_tiles_y)
        
        # Level
        self.LevelSelection = LevelSelection(self.scale_images)

        
        # load the sounds
        self.not_enough_money_sound = pygame.mixer.Sound("sound/not_enough_money.mp3")
        self.gameover_sound = pygame.mixer.Sound("sound/game_over.mp3")
        self.game_win_sound = pygame.mixer.Sound("sound/game_win.mp3")
        
        self.shoot_sound1 = pygame.mixer.Sound("sound/shoot_tower1.mp3")
        self.shoot_sound2 = pygame.mixer.Sound("sound/shoot_tower2.mp3")
        self.shoot_sound3 = pygame.mixer.Sound("sound/shoot_tower3.mp3")
        self.hoverSound = pygame.mixer.Sound("sound/button_hover.mp3")
        self.clickSound = pygame.mixer.Sound("sound/button_click.mp3")
        self.clickSoundRelease = pygame.mixer.Sound("sound/button_click_released.mp3")
        
        # volume
        vol = 0.3
        
        # load and scale all images
        tiles = ["images/tileset/grass_tile.png", "images/tileset/way_crossing.png", "images/tileset/way_down_left.png", "images/tileset/way_left_up.png", "images/tileset/way_right_down.png", "images/tileset/way_side.png", "images/tileset/way_three_down.png", "images/tileset/way_three_left.png", "images/tileset/way_three_right.png", "images/tileset/way_three_up.png", "images/tileset/way_up.png", "images/tileset/way_up_right.png"]
        self.tile_images = []
        for tile in tiles:
             self.tile_images.append(pygame.transform.scale(pygame.image.load(tile), self.scale_images))
        self.image_info_field = pygame.image.load("images/tileset/info_field.png")
        self.image_info_field = pygame.transform.scale(self.image_info_field, (self.x_scale-self.tile_x_scale, self.tile_y_scale))       
        self.image_select = pygame.image.load("images/tileset/select_field.png")
        self.image_select = pygame.transform.scale(self.image_select, self.scale_images)
        
        # window images
        self.image_win_temp = pygame.image.load("images/window/window_template.png")
        self.image_win_temp = pygame.transform.scale(self.image_win_temp, (1.0*self.image_win_temp.get_width(), 1.4*self.image_win_temp.get_height()))
        self.image_win_temp_hl = pygame.image.load("images/window/window_template_headline.png")
        self.image_win_temp_hl = pygame.transform.scale(self.image_win_temp_hl, (1.0*self.image_win_temp_hl.get_width(), 1.3*self.image_win_temp_hl.get_height()))
        self.image_button = pygame.image.load("images/window/button.png")
        self.image_button_click = pygame.image.load("images/window/button_click.png")
        self.image_button_hover = pygame.image.load("images/window/button_hover.png")
        
        # tower images
        self.image_tower1_free = pygame.image.load("images/tower/tower_free.png")
        self.image_tower1_free_menu = pygame.transform.scale(self.image_tower1_free, (self.image_info_field.get_width()*0.6, self.image_info_field.get_width()*0.6))

        self.image_tower1_upgrade_free = pygame.image.load("images/tower/tower1_upgrade_free.png")
        self.image_tower1_upgrade_free_menu = pygame.transform.scale(self.image_tower1_upgrade_free, (self.image_info_field.get_width()*0.6, self.image_info_field.get_width()*0.6))
        
        self.image_tower1_upgrade2_free = pygame.image.load("images/tower/tower1_upgrade2_free.png")
        self.image_tower1_upgrade2_free_menu = pygame.transform.scale(self.image_tower1_upgrade2_free, (self.image_info_field.get_width()*0.6, self.image_info_field.get_width()*0.6))
        
        
        self.image_tower2_free = pygame.image.load("images/tower/tower_green.png")
        self.image_tower2_free_menu = pygame.transform.scale(self.image_tower2_free, (self.scale_images[0]*2, self.scale_images[1]*2))

        self.image_tower2_upgrade_free = pygame.image.load("images/tower/tower_green_upgrade.png")
        self.image_tower2_upgrade_free_menu = pygame.transform.scale(self.image_tower2_upgrade_free, (self.image_info_field.get_width()*0.6, self.image_info_field.get_width()*0.6))
        
        self.image_tower2_upgrade2_free = pygame.image.load("images/tower/tower_green_upgrade2.png")
        self.image_tower2_upgrade2_free_menu = pygame.transform.scale(self.image_tower2_upgrade2_free, (self.image_info_field.get_width()*0.6, self.image_info_field.get_width()*0.6))
        
        
        self.image_tower3_free = pygame.image.load("images/tower/tower_cristal.png")
        self.image_tower3_free_menu = pygame.transform.scale(self.image_tower3_free, (self.image_info_field.get_width()*0.6, self.image_info_field.get_width()*0.6))

        self.image_tower3_upgrade_free = pygame.image.load("images/tower/tower_cristal_upgrade.png")
        self.image_tower3_upgrade_free_menu = pygame.transform.scale(self.image_tower3_upgrade_free, (self.image_info_field.get_width()*0.6, self.image_info_field.get_width()*0.6))
        
        self.image_tower3_upgrade2_free = pygame.image.load("images/tower/tower_cristal_upgrade2.png")
        self.image_tower3_upgrade2_free_menu = pygame.transform.scale(self.image_tower3_upgrade2_free, (self.image_info_field.get_width()*0.6, self.image_info_field.get_width()*0.6))

        # menu to buy a tower
        self.tower_position_x_1 = (38/64) * self.scale_images[0] + self.tile_x_scale
        self.tower_position_x_2 = (103/64) * self.scale_images[0] + self.tile_x_scale
        self.tower_position_y_1 = (168/64) * self.scale_images[1]
        self.tower_position_y_2 = self.tower_position_y_1 + self.image_tower1_free_menu.get_height()
        self.tower_position_y_3 = self.tower_position_y_2 + self.image_tower1_free_menu.get_height() + 40
        
        # position for label
        self.label_Tower1_x = (60/64) * self.scale_images[0] + self.tile_x_scale
        self.labelTower1_Upgrade_x = (125/64) * self.scale_images[0] + self.tile_x_scale
        self.label_Tower1_y_1 = (230/64) * self.scale_images[1]
    
        # some important attributes for game
        self.health_user = 100
        self.user_highscore = 0
        self.money_user = 35
        self.tower_list = []
        self.select = None  
        
        self.levelIndex = None
        self.maxLevel = 0
        
        # append towers to menu
        self.towers_menu_original = []
        self.towers_menu = []
        self.towers_menu_original.append(Tower(self.tower_position_x_1, self.tower_position_y_1, [self.image_tower1_free_menu, self.image_tower1_upgrade_free_menu, self.image_tower1_upgrade2_free_menu], 0, 2*self.scale_images[0], 50, 100, self.shoot_sound1, 20))
        self.towers_menu_original.append(Tower(self.tower_position_x_1, self.tower_position_y_2, [self.image_tower2_free_menu, self.image_tower2_upgrade_free_menu, self.image_tower2_upgrade2_free_menu], 0, 1.75*self.scale_images[0], 175, 175, self.shoot_sound2, 40))
        self.towers_menu_original.append(Tower(self.tower_position_x_1, self.tower_position_y_3, [self.image_tower3_free_menu, self.image_tower3_upgrade_free_menu, self.image_tower3_upgrade2_free_menu], 0, 1.5*self.scale_images[0], 300, 250, self.shoot_sound3, 60))
        
        self.win = False
        
    # define a main function
    def main(self, levelIndex, muteMusic, muteSound):
         
        # FPS for game-loop
        FPS = 60
        fpsClock = pygame.time.Clock()
        running = True
        
        # reset the level
        self.LevelSelection.resetLevel()
        self.level = self.LevelSelection.getLevel(levelIndex[0], levelIndex[1])
        enemies = []
        count = 149
        countEnemiesSpawn = 0
                            
        # information if one tower is hovered
        self.hoverTower = None
        
        # data for highscore
        self.starttime = time.time()
        self.countEnemiesHit = 0
        
        # some important attributes for game
        self.health_user = 100
        self.user_highscore = 0
        self.money_user = self.level.startMoney
        self.tower_list = []
        self.select = None
        self.win = False
        
        self.levelIndex = levelIndex

        # init font
        pygame.font.init()
        
        # load and play music
        pygame.mixer.music.load("sound/game_music.mp3")
        pygame.mixer.music.set_volume(muteMusic)
        pygame.mixer.music.play(-1)
        
        # set volume
        self.not_enough_money_sound.set_volume(muteSound)
        self.gameover_sound.set_volume(muteSound)
        self.game_win_sound.set_volume(muteSound)
        self.shoot_sound1.set_volume(muteSound)
        self.shoot_sound2.set_volume(muteSound)
        self.shoot_sound3.set_volume(muteSound)
        self.hoverSound.set_volume(muteSound)
        self.clickSound.set_volume(muteSound)
        self.clickSoundRelease.set_volume(muteSound)
        
        # create screen
        screen = self.screen
        
        # info field to buy towers
        screen.blit(self.image_info_field, (self.tile_x_scale,0))
        #self.addTowersToMenu(screen)
        
        # label for important information
        self.labelHealth = self.font_futura(18).render(str(self.health_user), True, (255, 255, 255))
        screen.blit(self.labelHealth, (self.tile_x_scale + 80 / 640 * self.tile_y_scale, 62 / 640 * self.tile_y_scale))
        self.labelMoney = self.font_futura(18).render(str(self.money_user), True, (255, 255, 255))
        screen.blit(self.labelMoney, (self.tile_x_scale + 80 / 640 * self.tile_y_scale, 98 / 640 * self.tile_y_scale))
        self.labelHighscore = self.font_futura(18).render("Highscore: "+str(self.user_highscore), True, (255, 255, 255))
        screen.blit(self.labelHighscore, (10, 10))
        
        # data for spawing enemies
        enemies = []
        count = 149
        countEnemiesSpawn = 0
        
        # information if one tower is hovered
        self.hoverTower = None
        
        # data for highscore
        self.starttime = time.time()
        self.countEnemiesHit = 0
        
        # get the current way
        waypoints = self.level.waypoints
        finalWaypoints = []
        # transform it in one list
        for way in waypoints:
            finalWaypoints += way
                
        # main loop
        while running:
            pygame.event.pump()
            endtime = time.time()
            
            # set actual highscore
            self.user_highscore = int((endtime - self.starttime)) * 5 + self.countEnemiesHit*30
            
            # index for spawning an enemy
            count += 1

            # draw the background
            self.drawBackground(screen, finalWaypoints)            
            # check for game over
            if self.health_user == 0:
                running = self.gameEnd(screen, endtime)
                # reset the level
                self.LevelSelection.resetLevel()
                self.level = self.LevelSelection.getLevel(levelIndex[0], levelIndex[1])
                enemies = []
                count = 149
                countEnemiesSpawn = 0
                            
                # information if one tower is hovered
                self.hoverTower = None
                            
                # data for highscore
                self.starttime = time.time()
                self.countEnemiesHit = 0
                            
                # some important attributes for game
                self.health_user = 100
                self.user_highscore = 0
                self.money_user = self.level.startMoney
                self.tower_list = []
                self.select = None
                self.win = False
                if not running:
                    return True
                else: 
                    pygame.mixer.music.unpause()
                    continue
        
            elif len(enemies) == 0 and self.level.enemyIndex >= len(self.level.enemies) and self.levelIndex[1] == 0:
                    self.win = True
                    running = self.gameEnd(screen, time.time())
                    # reset the level
                    self.level = self.LevelSelection.getLevel(self.levelIndex[0], self.levelIndex[1])
                    self.LevelSelection.resetLevel()
                    enemies = []
                    count = 149
                    countEnemiesSpawn = 0
                                        
                    # information if one tower is hovered
                    self.hoverTower = None
                                        
                    # data for highscore
                    self.starttime = time.time()
                    self.countEnemiesHit = 0
                                        
                    # some important attributes for game
                    self.health_user = 100
                    self.user_highscore = 0
                    self.money_user = self.level.startMoney
                    self.tower_list = []
                    self.select = None
                    self.win = False
                        
                    waypoints = self.level.waypoints
                    finalWaypoints = []
                    # transform it in one list
                    for way in waypoints:
                        finalWaypoints += way
                        
                    if not running:
                        return True
                        break
                    else: 
                        pygame.mixer.music.unpause()
                        continue
                
            # move the enemies
            enemies = self.moveEnemies(enemies, screen)

            # possibly spawn new enemies
            if count >= 150:
                if not countEnemiesSpawn > 20:
                    countEnemiesSpawn += 1
                    newEnemy = (self.level.getNextEnemy())
                    if levelIndex[1] == 1 and len(enemies) == 0:
                        while newEnemy == None:
                            newEnemy = (self.level.getNextEnemy())
                    if not newEnemy == None:
                        enemies.append(newEnemy)            
                    count = 0

                else:
                    countEnemiesSpawn = 0
                    count = count/2

            # tower possibly shoot
            for tower in self.tower_list:
                tower.checkForEnemy(enemies)
            
            # get all current events
            events = pygame.event.get()
            for event in events:
                # if quit, quit
                if event.type == pygame.QUIT:
                    return False
                
                # if hover over a tower, show the range of the tower
                elif event.type == pygame.MOUSEMOTION:
                    x, y = pygame.mouse.get_pos()
                    hoverTower = None
                    for tower in self.towers_menu:
                        if(tower.x <= x and tower.x + (self.scale_images[0]*2) >= x and tower.y <= y and tower.y + (self.scale_images[1]*2) >= y and self.select != None):
                            hoverTower = tower
                            
                    self.hoverTower = hoverTower
                            
                # if mouseclick:
                elif event.type == pygame.MOUSEBUTTONUP:
                    x, y = pygame.mouse.get_pos()
                    
                    # place tower if needed
                    self.checkForTower(x, y, screen)
                    
                    # highlight clicked field if needed
                    position = self.findeSelectField(screen, finalWaypoints, x//self.scale_images[0], y//self.scale_images[1])
                    
                # if 'P' then pause   
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_p or event.key == pygame.K_ESCAPE:     
                        buttonClick = self.paused(screen)
                        if buttonClick == 3:
                            return True
                            running = False
                            break
                        elif buttonClick == 2: 
                            # reset the level
                            self.LevelSelection.resetLevel()
                            self.level = self.LevelSelection.getLevel(levelIndex[0], levelIndex[1])
                            enemies = []
                            count = 149
                            countEnemiesSpawn = 0
                                        
                            # information if one tower is hovered
                            self.hoverTower = None
                                        
                            # data for highscore
                            self.starttime = time.time()
                            self.countEnemiesHit = 0
                                        
                            # some important attributes for game
                            self.health_user = 100
                            self.user_highscore = 0
                            self.money_user = self.level.startMoney
                            self.tower_list = []
                            self.select = None
                            continue
                        
                
            # update and show changes
            pygame.display.update()
            fpsClock.tick(FPS)

    # game over function
    def gameEnd(self, gameDisplay, endtime):
        # pause the music
        text = "Verloren!"
        sound = self.gameover_sound
        if self.win:
            text = "Gewonnen!"
            sound = self.game_win_sound
        pygame.mixer.music.pause()
        pause = True
        go_clock = pygame.time.Clock()
        
        # play sound and darken the screen in some steps
        alpha = 20
        pygame.mixer.Sound.play(sound)
        for i in range(1, 5):
            s = pygame.Surface((self.x_scale,self.tile_y_scale))
            s.set_alpha(alpha)
            alpha += 20
            s.fill((0, 0, 0))
            gameDisplay.blit(s, (0,0))
            pygame.display.update()
            time.sleep(0.2)

        # show window
        gameDisplay.blit(self.image_win_temp, (self.x_scale/2-self.image_win_temp.get_width()/2, self.tile_y_scale/2 - self.image_win_temp.get_height()/2))
        gameDisplay.blit(self.image_win_temp_hl, (self.x_scale/2-self.image_win_temp.get_width()/2, self.tile_y_scale/2 - self.image_win_temp.get_height()/2))
        
        # window title
        textTitle = self.font_futura(50).render(text, True, (255, 255, 255))
        titleRect = textTitle.get_rect()
        titleRect.center = (self.x_scale/2, self.tile_y_scale/2 - self.image_win_temp.get_height()/2 + 60)
        gameDisplay.blit(textTitle, titleRect)
        pygame.display.update()     
        time.sleep(0.2)
        
        # calculate current highscore
        x_value = self.x_scale/2 - self.image_win_temp.get_width()/2 + 330
        y_value = self.tile_y_scale/2 - self.image_win_temp.get_height()/2 + 150
        
        label_time = self.font_futura(20).render("Zeitbonus: ", True, (0, 0, 0))
        label_timevalue = self.font_futura(20).render(str(int(endtime-self.starttime))+" * 5", True, (0, 0, 0))
        gameDisplay.blit(label_time, (x_value, y_value))
        gameDisplay.blit(label_timevalue, (x_value + 200, y_value))
        pygame.display.update()     
        time.sleep(0.8)
        
        label_enemies = self.font_futura(20).render("Gegnerbonus: ", True, (0, 0, 0))
        label_enemiesvalue = self.font_futura(20).render(str(self.countEnemiesHit)+" * 30", True, (0, 0, 0))
        gameDisplay.blit(label_enemies, (x_value, y_value + 40))
        gameDisplay.blit(label_enemiesvalue, (x_value + 200, y_value + 40))
        pygame.display.update()     
        time.sleep(0.8)
        
        label_highscore = self.font_futura(20).render("Highscore: ", True, (0, 0, 0))
        label_highscorevalue = self.font_futura(20).render(str(self.user_highscore), True, (0, 0, 0))
        gameDisplay.blit(label_highscore, (x_value, y_value + 80))
        gameDisplay.blit(label_highscorevalue, (x_value + 200, y_value + 80))
        pygame.display.update()     
        time.sleep(0.8)
        
        # calculate if player is in top ten
        highscores = self.readHighscorelist()
        place = self.calculatePlace(highscores)

        # display top ten
        textfield = None
        label_titleTopTen = self.font_futura(20).render("Top Ten: ", True, (0, 0, 0))
        gameDisplay.blit(label_titleTopTen, (x_value, y_value + 135))
        j = 0
        margin = 135 + 40
        for i in range(0, 10):
            if i == place:
                # player can input his name
                label_score = self.font_futura(20).render(str(self.user_highscore), True, (0, 0, 0))
                label_num = self.font_futura(20).render(str(i+1)+". ", True, (0, 0, 0))
                textfield = InputBox(x_value + 20, y_value + margin, 140, 35, self.font_futura(20), "Eingabe...")
                textfield.change_color((255, 255, 255), (0, 0, 0), (255, 199, 58))
                gameDisplay.blit(label_num, (x_value, y_value + margin + 5))
                gameDisplay.blit(label_score, (x_value  + 200, y_value + margin + 5))
            else: 
                label_name = self.font_futura(20).render(str(i+1)+". "+highscores[0][j], True, (0, 0, 0))
                label_score = self.font_futura(20).render(highscores[1][j], True, (0, 0, 0))
                gameDisplay.blit(label_score, (x_value + 200, y_value + margin + 5))
                gameDisplay.blit(label_name, (x_value, y_value + margin + 5))
                j += 1
            margin = margin + 35
        
        # button positions
        x_pos1_button = self.x_scale/2 - self.image_button.get_width()/2
        x_pos2_button = self.x_scale/2 + self.image_button.get_width()/2
                    
        y_pos1_button1 = self.tile_y_scale/2+self.image_win_temp.get_height()/2-32-self.image_button.get_height()
        y_pos2_button1 = self.tile_y_scale/2+self.image_win_temp.get_height()/2-32+self.image_button.get_height()
                    
        y_pos1_button2 = self.tile_y_scale/2+self.image_win_temp.get_height()/2-52-2*self.image_button.get_height()
        y_pos2_button2 = self.tile_y_scale/2+self.image_win_temp.get_height()/2-52-1*self.image_button.get_height()
        
        x_pos1_button2 = x_pos1_button
        x_pos2_button2 = x_pos2_button
        
                    # self.level = self.LevelSelection.getLevel(levelIndex[0], levelIndex[1])    
        # display buttons
        image_button1 = self.image_button
        image_button2 = self.image_button
        image_button3 = self.image_button
        
        image_button2_clicked = self.image_button_click
        image_button2_hover = self.image_button_hover
        image_button2_normal = self.image_button
        
        label_again = self.font_futura(20).render("Nochmal", True, (0, 0, 0))
        label_backToMenue = self.font_futura(20).render("Zurück zum Hauptmenü", True, (0, 0, 0))
        againRect = label_again.get_rect()
        againRect.center = ((self.x_scale/2, self.tile_y_scale/2+self.image_win_temp.get_height()/2-50-3*self.image_button.get_height()/2))
        label_next = self.font_futura(20).render("Weiter", True, (0, 0, 0))
        nextRect = label_next.get_rect()
        if self.win:
            image_button2 = pygame.transform.scale(image_button2, (image_button2.get_width()/2-10, image_button2.get_height()))
            againRect.center = ((self.x_scale/2 - image_button2.get_width()/2 - 10, self.tile_y_scale/2+self.image_win_temp.get_height()/2-50-3*self.image_button.get_height()/2))
            nextRect.center = ((self.x_scale/2 + image_button2.get_width()/2 + 10, self.tile_y_scale/2+self.image_win_temp.get_height()/2-50-3*self.image_button.get_height()/2))
            image_button3 = image_button2
            x_pos1_button2 = self.x_scale/2 - image_button1.get_width()/2
            x_pos2_button2 = x_pos1_button2 + image_button2.get_width()
            
            image_button2_clicked = pygame.transform.scale(self.image_button_click, (image_button2.get_width(), image_button2.get_height()))
            image_button2_hover = pygame.transform.scale(self.image_button_hover, (image_button2.get_width(), image_button2.get_height()))
            image_button2_normal = pygame.transform.scale(self.image_button, (image_button2.get_width(), image_button2.get_height()))
            
        backToMenueRect = label_backToMenue.get_rect()
        backToMenueRect.center = ((self.x_scale/2, self.tile_y_scale/2+self.image_win_temp.get_height()/2-30-self.image_button.get_height()/2))
        
        x_pos1_button3 = self.x_scale/2 + 10
        x_pos2_button3 = x_pos1_button3 + image_button2.get_width()
        
        gameDisplay.blit(image_button1, (x_pos1_button, y_pos1_button1))
        gameDisplay.blit(image_button2, (x_pos1_button2, y_pos1_button2))
        gameDisplay.blit(label_again, againRect)
        gameDisplay.blit(label_backToMenue, backToMenueRect)
        if self.win:
            gameDisplay.blit(image_button3, (x_pos1_button3, y_pos1_button2))
            gameDisplay.blit(label_next, nextRect)
        
        # Spiel beenden?
        again = True
        
        # button events
        hover = None
        click = None
        fail = False
        while pause:
            
            # draw the inputfield
            if not textfield == None:
                textfield.draw(gameDisplay)
            
            # check the events
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return False
                
                write = False
                if not textfield == None:
                    write = textfield.handle_event(event)
                            
                if event.type == pygame.MOUSEMOTION:
                    x, y = pygame.mouse.get_pos()
                    
                    # check if hover button 1
                    if x >= x_pos1_button and x <= x_pos2_button and y >= y_pos1_button1 and y <= y_pos2_button1:
                        image_button2 = image_button2_normal
                        image_button3 = image_button2_normal
                        image_button1 = self.image_button_hover
                        if hover != 1 and click == None:
                            pygame.mixer.Sound.play(self.hoverSound)
                            hover = 1
                    
                    
                    else: 
                        image_button1 = self.image_button
                            
                            # check if hover button 2
                        if x >= x_pos1_button2 and x <= x_pos2_button2 and y >= y_pos1_button2 and y <= y_pos2_button2:
                            image_button2 = image_button2_hover
                            image_button3 = image_button2_normal
                            if hover != 2 and click == None:
                                pygame.mixer.Sound.play(self.hoverSound)
                                hover = 2
                        else:      
                            image_button2 = image_button2_normal
                            if not self.win: 
                                hover = None
                            else: 
                                # check if hover button 3
                                if x >= x_pos1_button3 and x <= x_pos2_button3 and y >= y_pos1_button2 and y <= y_pos2_button2:
                                    image_button3 = image_button2_hover
                                    if hover != 3 and click == None:
                                        pygame.mixer.Sound.play(self.hoverSound)
                                        hover = 3
                                else:      
                                    hover = None
                                    image_button3 = image_button2_normal
                                
                left, middle, right = pygame.mouse.get_pressed()
                if left:
                    x, y = pygame.mouse.get_pos()
                    
                    # check if click button 1 (back to menue)
                    if x >= x_pos1_button and x <= x_pos2_button and y >= y_pos1_button1 and y <= y_pos2_button1:
                        image_button1 = self.image_button_click
                        again = False
                        if not click == 1:
                            click = 1   
                            pygame.mixer.Sound.play(self.clickSound)
                    
                    # check if click button 2 (again)
                    elif x >= x_pos1_button2 and x <= x_pos2_button2 and y >= y_pos1_button2 and y <= y_pos2_button2:
                        pygame.mixer.Sound.play(self.clickSound)
                        if not click == 2:
                            click = 2
                            image_button2 = image_button2_clicked
                    
                    # check if click button 3 (next)
                    elif self.win:
                        if x >= x_pos1_button3 and x <= x_pos2_button3 and y >= y_pos1_button2 and y <= y_pos2_button2:
                            pygame.mixer.Sound.play(self.clickSound)
                            if not click == 3:
                                click = 3
                                image_button3 = image_button2_clicked
                                if self.levelIndex[0] < len(self.LevelSelection.level[0])-1:
                                    self.levelIndex[0] += 1
                                else: 
                                    fail = True
                else:
                    if click in [1, 2, 3]:
                        if fail:
                            pygame.mixer.Sound.play(self.not_enough_money_sound)
                            click = None
                            fail = False
                        else: 
                            if not textfield == None:
                                self.saveHighscorelist(highscores, textfield.text, self.user_highscore, place)
                            else:
                                self.saveHighscorelist(highscores, None, None, None)
                            pygame.mixer.Sound.play(self.clickSoundRelease)
                            pause = False
                            break

                # draw the buttons again                    
                gameDisplay.blit(image_button1, (x_pos1_button, y_pos1_button1))
                gameDisplay.blit(image_button2, (x_pos1_button2, y_pos1_button2))
                gameDisplay.blit(label_again, againRect)
                gameDisplay.blit(label_backToMenue, backToMenueRect)
                if self.win:
                    gameDisplay.blit(image_button3, (x_pos1_button3, y_pos1_button2))
                    gameDisplay.blit(label_next, nextRect)
                
            # update the screen
            pygame.display.update()
            go_clock.tick(30)
            
        del go_clock
        
        # end of loop
        self.starttime += (time.time() - endtime)
        return again
       
    # pause function
    def paused(self, gameDisplay):
        # don't count the time here
        starttime = time.time()
        pause = True
        pause_clock = pygame.time.Clock()
        
        # pause music
        pygame.mixer.music.pause()

        # show pause
        s = pygame.Surface((self.x_scale,self.tile_y_scale))
        s.set_alpha(128)
        s.fill((0, 0, 0))
        gameDisplay.blit(s, (0,0))

        
        image_win_temp = pygame.transform.scale(self.image_win_temp, (0.5*self.x_scale, self.image_win_temp.get_width()/self.x_scale * self.image_win_temp.get_height()))
        gameDisplay.blit(image_win_temp, (self.x_scale/2 - image_win_temp.get_width()/2, self.tile_y_scale/2 - image_win_temp.get_height()/2))
        image_win_temp_hl = pygame.transform.scale(self.image_win_temp_hl, (0.5*self.x_scale, self.image_win_temp_hl.get_width()/self.x_scale * self.image_win_temp_hl.get_height()))
        gameDisplay.blit(image_win_temp_hl, (self.x_scale/2 - image_win_temp_hl.get_width()/2, self.tile_y_scale/2 - image_win_temp_hl.get_height()/2))
        
        textSurfaceObj = self.font_futura(25).render("Pause", True, (255, 255, 255))
        textRectObj = textSurfaceObj.get_rect()
        textRectObj.center = (self.x_scale/2, self.tile_y_scale/2 - image_win_temp_hl.get_height()/2 + 35)
        gameDisplay.blit(textSurfaceObj, textRectObj)
        
        # button positions
        button_margin = 35
        button_dist_center = 20
        x_pos1_button = self.x_scale/2 - self.image_button.get_width()/2
        x_pos2_button = self.x_scale/2 + self.image_button.get_width()/2
        
        y_pos1_button2 = self.tile_y_scale/2 - self.image_button.get_height()/2  + button_dist_center
        y_pos2_button2 = y_pos1_button2 + self.image_button.get_height()           
        
        y_pos1_button3 = y_pos2_button2 + button_margin
        y_pos2_button3 = y_pos1_button3 + self.image_button.get_height()
        
        y_pos1_button1 = y_pos1_button2 - self.image_button.get_height() - button_margin
        y_pos2_button1 = y_pos1_button1 + self.image_button.get_height()
        
        # display buttons
        gameDisplay.blit(self.image_button, (x_pos1_button, y_pos1_button1))
        gameDisplay.blit(self.image_button, (x_pos1_button, y_pos1_button2))
        gameDisplay.blit(self.image_button, (x_pos1_button, y_pos1_button3))
        
        label_again = self.font_futura(20).render("Neustart", True, (0, 0, 0))
        againRect = label_again.get_rect()
        againRect.center = ((x_pos1_button + x_pos2_button)/2, (y_pos1_button2+y_pos2_button2)/2)
        label_resume = self.font_futura(20).render("Weiter", True, (0, 0, 0))
        resumeRect = label_resume.get_rect()
        resumeRect.center = ((x_pos1_button + x_pos2_button)/2, (y_pos1_button1+y_pos2_button1)/2)
        label_backToMenue = self.font_futura(20).render("Zurück zum Hauptmenü", True, (0, 0, 0))
        backToMenueRect = label_backToMenue.get_rect()
        backToMenueRect.center = ((x_pos1_button + x_pos2_button)/2, (y_pos1_button3+y_pos2_button3)/2)
        
        gameDisplay.blit(label_resume, resumeRect)
        gameDisplay.blit(label_again, againRect)
        gameDisplay.blit(label_backToMenue, backToMenueRect)
        
        click = None
        hover = None
        
        image_button1 = self.image_button
        image_button2 = self.image_button
        image_button3 = self.image_button
        # wait for event
        while pause:
            
            for event in pygame.event.get():
    
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_p or event.key == pygame.K_ESCAPE:
                        pause = False
                        break
                    
                if event.type == pygame.MOUSEMOTION:
                    x, y = pygame.mouse.get_pos()
                    
                    # check if hover button 1
                    if x >= x_pos1_button and x <= x_pos2_button and y >= y_pos1_button1 and y <= y_pos2_button1:
                        image_button1 = self.image_button_hover
                        if hover != 1 and click == None:
                            pygame.mixer.Sound.play(self.hoverSound)
                            hover = 1
                    
                    else: 
                        image_button1 = self.image_button
                        
                        # check if hover button 2
                        if x >= x_pos1_button and x <= x_pos2_button and y >= y_pos1_button2 and y <= y_pos2_button2:
                            image_button2 = self.image_button_hover
                            if hover != 2 and click == None:
                                pygame.mixer.Sound.play(self.hoverSound)
                                hover = 2
                            
                        # check if hover button 3
                        else:
                            image_button2 = self.image_button
                            if x >= x_pos1_button and x <= x_pos2_button and y >= y_pos1_button3 and y <= y_pos2_button3:
                                image_button3 = self.image_button_hover
                                if hover != 3 and click == None:
                                    pygame.mixer.Sound.play(self.hoverSound)
                                    hover = 3
                            else:
                                image_button3 = self.image_button
                                hover = None

                            
                left, middle, right = pygame.mouse.get_pressed()
                if left:
                    x, y = pygame.mouse.get_pos()

                    # check if click button 1
                    if x >= x_pos1_button and x <= x_pos2_button and y >= y_pos1_button1 and y <= y_pos2_button1:
                        image_button1 = self.image_button_click
                        if not click == 1:
                            pygame.mixer.Sound.play(self.clickSound)
                            click = 1
                    
                    # check if click button 2
                    elif x >= x_pos1_button and x <= x_pos2_button and y >= y_pos1_button2 and y <= y_pos2_button2:
                        image_button2 = self.image_button_click
                        if not click == 2:
                            pygame.mixer.Sound.play(self.clickSound)
                            click = 2
                        
                    # check if click button 3
                    elif x >= x_pos1_button and x <= x_pos2_button and y >= y_pos1_button3 and y <= y_pos2_button3:
                        image_button3 = self.image_button_click
                        if not click == 3:
                            pygame.mixer.Sound.play(self.clickSound)
                            click = 3
                else:
                    if click == 1 or click == 2 or click == 3:
                        pygame.mixer.Sound.play(self.clickSoundRelease)
                        pause = False
                        break
                    
            # draw the buttons again  
            gameDisplay.blit(image_win_temp, (self.x_scale/2 - image_win_temp.get_width()/2, self.tile_y_scale/2 - image_win_temp.get_height()/2))
            gameDisplay.blit(image_win_temp_hl, (self.x_scale/2 - image_win_temp_hl.get_width()/2, self.tile_y_scale/2 - image_win_temp_hl.get_height()/2))
            gameDisplay.blit(textSurfaceObj, textRectObj)
                          
            gameDisplay.blit(image_button1, (x_pos1_button, y_pos1_button1))
            gameDisplay.blit(image_button2, (x_pos1_button, y_pos1_button2))
            gameDisplay.blit(image_button3, (x_pos1_button, y_pos1_button3))
            gameDisplay.blit(label_resume, resumeRect)
            gameDisplay.blit(label_again, againRect)
            gameDisplay.blit(label_backToMenue, backToMenueRect)
            pygame.display.update()
        
        # game goes on
        pygame.mixer.music.unpause()
        self.starttime += (time.time() - starttime)
        return click
    
    # read and decrypt the Highscorelist
    def readHighscorelist(self):
          
        # opening the encrypted file
        with open('Highscore.txt', 'rb') as enc_file:
            encrypted = enc_file.read()
          
        # key
        key = b"1w2JFZKnG68Prrnblx_kbDJq4jTQ-6zpHbapKzzxv84="       
        fernet = Fernet(key)
        
        # decrypt
        highscoreRaw = fernet.decrypt(encrypted).decode('unicode_escape')
        
        # from string to array
        highscore_split = highscoreRaw.split("\n")
        
        for i in range(0, len(highscore_split)):
            highscore_split[i] = highscore_split[i].rsplit("\r", 1)[0]
            
        highscore_names = highscore_split[0:10]
        highscore_points = highscore_split[11:21]
        highscore_list = [highscore_names, highscore_points]
        
        self.maxLevel = int(highscore_split[len(highscore_split)-1])

        enc_file.close()
        return highscore_list
        
    # save and encrypt the Highscorelist
    def saveHighscorelist(self, highscores, name, score, place):
        # from array to string
        highscoreNames = ""
        highscoreNums = ""
        j = 0
        for i in range(0, len(highscores[0])):
            if i == place:
                highscoreNames += name
                highscoreNums += str(score)
            else:
                highscoreNames += highscores[0][j]
                highscoreNums += highscores[1][j]
                j += 1
            highscoreNames += "\n"
            highscoreNums += "\n"                
        
        if self.win:
            if self.maxLevel < (self.levelIndex[0]+1):
                self.maxLevel = (self.levelIndex[0]+1)
        highscoreRaw = (highscoreNames + "\n" + highscoreNums + "\n" + str(self.maxLevel))
        highscoreRaw = highscoreRaw.encode('UTF-8')
          
        # key
        key = b"1w2JFZKnG68Prrnblx_kbDJq4jTQ-6zpHbapKzzxv84="       
        fernet = Fernet(key)
        
        # encrypt the data
        highscoreRaw = fernet.encrypt(highscoreRaw)
        # write in file
        with open('Highscore.txt', 'wb') as enc_file:
            enc_file.write(highscoreRaw)

        enc_file.close() 
        
    # calculate the current place, if possible    
    def calculatePlace(self, highscore):
        values = highscore[1]
        for i in range(0, len(values)):
            if self.user_highscore > int(values[i]):
                return i
        return None
    
    # drawing the screen
    def drawBackground(self, screen, waypoints):
        # draw green fields everywhere
        image = self.tile_images[0]       
        for row in range(self.num_tiles_x):
            for column in range(self.num_tiles_y):
                screen.blit(image, (row * self.tile_x_scale/self.num_tiles_x, column * self.tile_y_scale/self.num_tiles_y))

        # draw the waypoints
        for t in waypoints:
            index = t[2]
            image_way = self.tile_images[index]
            screen.blit(image_way, (t[0] * self.tile_x_scale/self.num_tiles_x, t[1] * self.tile_y_scale/self.num_tiles_y ))

        # draw the tower
        for tower in self.tower_list:
            screen.blit(tower.image, (tower.x, tower.y))
        
        tower_select = None
        # draw select if something is selected
        if not self.select == None:
            screen.blit(self.image_select, (self.select[0] * (self.tile_x_scale/self.num_tiles_x), self.select[1] * (self.tile_y_scale/self.num_tiles_y)))
            # draw the range from the current tower
            for t in self.tower_list:
                if t.x == self.select[0]*self.scale_images[0] and t.y == self.select[1]*self.scale_images[1]:
                    if self.hoverTower == None:
                        pygame.draw.circle(screen, (55, 177, 233), (t.x+self.scale_images[0]/2, t.y+self.scale_images[1]/2), t.distance, 3)
                    tower_select = t
            if not self.hoverTower == None:
                pygame.draw.circle(screen, (55, 177, 233), (self.select[0] * (self.tile_x_scale/self.num_tiles_x)+self.tile_images[0].get_width()/2, self.select[1] * (self.tile_y_scale/self.num_tiles_y)+self.tile_images[0].get_height()/2), self.hoverTower.distance, 3)

        # draw the information on the side
        screen.blit(self.image_info_field, (self.tile_x_scale, 0))

        self.labelHealth = self.font_futura(18).render(str(self.health_user), True, (255, 255, 255))
        screen.blit(self.labelHealth, (self.tile_x_scale + 80 / 640 * self.tile_y_scale, 62 / 640 * self.tile_y_scale))
        self.labelMoney = self.font_futura(18).render(str(self.money_user), True, (255, 255, 255))
        screen.blit(self.labelMoney, (self.tile_x_scale + 80 / 640 * self.tile_y_scale, 98 / 640 * self.tile_y_scale))
        
        # no tower selected but field
        if tower_select == None and self.select != None:
            # show all tower in level 0
            self.towers_menu = self.towers_menu_original
        # if one tower is selected, show only upgrade from tower
        elif tower_select != None:
            upgradeImages = copy.copy(tower_select.images)
            for i in range(0, len(upgradeImages)):
                upgradeImages[i] = pygame.transform.scale(upgradeImages[i], (upgradeImages[i].get_width()*2, upgradeImages[i].get_height()*2))
            upgradeTower = None
            self.towers_menu = []
            if tower_select.level < len(tower_select.images)-1:
                upgradeTower = copy.copy(tower_select)
                upgradeTower.upgrade()
                upgradeTower.image = upgradeImages[tower_select.level + 1]
                upgradeTower.images = upgradeImages
                upgradeTower.x = self.towers_menu_original[0].x
                upgradeTower.y = self.towers_menu_original[0].y
                self.towers_menu.append(upgradeTower)
        
        # draw the Highscore
        self.labelHighscore = self.font_futura(18).render("Highscore: "+str(self.user_highscore), True, (255, 255, 255))
        screen.blit(self.labelHighscore, (10, 10))
        
        # draw towers menu
        for tower in self.towers_menu:
            screen.blit(tower.image, (tower.x, tower.y))
            # label from tower
            label_price = self.font_futura(20).render("$ "+str(tower.value), True, (255, 255, 255))
            priceRect = label_price.get_rect()
            priceRect.center = (tower.x + tower.image.get_width()/2, tower.y + tower.image.get_height()+20)
            screen.blit(label_price, priceRect)

    # move the enemies
    def moveEnemies(self, enemies, screen):
        # get all enemies
        towers_temp = self.tower_list.copy()
        enemies_remaining = []
        for enemy in enemies:
            enemy.updateCooldown()
            # get new position
            newPos = enemy.getNewPosition()
            # if the end is arrived, player looses health
            if newPos == None:
                self.health_user -= math.ceil((enemy.health/enemy.maxHealth)*20)
                if self.health_user < 0:
                    self.health_user = 0
            # enemy dies, if it has no health and player gets money
            elif enemy.health <= 0:
                    self.money_user += enemy.money_value
                    self.countEnemiesHit += 1
            else:
                # draw the enemy
                screen.blit(enemy.image, ((newPos[0], newPos[1])))
                # draw health_bar
                health_bar = pygame.Rect(newPos[0]+100, newPos[1]+20, 70, 10)
                color_health_bar = (0, 255, 0)
                colorvalue = (enemy.health/enemy.maxHealth)
                
                red = int(255*(-2.5)*(colorvalue-1))
                if red > 255:
                    red = 255
                green = int(255*2*(colorvalue-0.1))
                if green > 255:
                    green = 255
                elif green < 0:
                    green = 0
                    
                        
                pygame.draw.rect(screen, (red, green, 0), [newPos[0] + (enemy.image.get_size()[0]-50)/2, newPos[1]-10, (enemy.health/enemy.maxHealth)*50, 5], 0)
                enemies_remaining.append(enemy) 

        return enemies_remaining

    # function when buying a tower
    def checkForTower(self, x, y, screen):
        for tower in self.towers_menu:
            # if clicked on an image
            if(tower.x <= x and tower.x + (self.scale_images[0]*2) >= x and tower.y <= y and tower.y + (self.scale_images[1]*2) >= y and self.select != None):
                # check if there already is a tower
                hasTower = False
                for t in self.tower_list:
                    if t.x == self.select[0]*self.scale_images[0] and t.y == self.select[1]*self.scale_images[1]:
                        # if yes, get the tower level
                        hasTower = True
                        levelCurrentTower = t.level
                        # if clicked tower in menue is higher, player gets the upgrade
                        if tower.level > levelCurrentTower and self.money_user >= tower.value:
                            self.money_user -= tower.value
                            t.upgrade()
                            
                            # show next upgrade in shop, if available
                            upgradeImages = copy.copy(t.images)
                            for i in range(0, len(upgradeImages)):
                                upgradeImages[i] = pygame.transform.scale(upgradeImages[i], (upgradeImages[i].get_width()*2, upgradeImages[i].get_height()*2))
                            upgradeTower = None
                            self.towers_menu = []
                            if t.level < len(t.images)-1:
                                upgradeTower = copy.copy(t)
                                upgradeTower.upgrade()
                                upgradeTower.images = upgradeImages
                                upgradeTower.x = self.towers_menu_original[0].x
                                upgradeTower.y = self.towers_menu_original[0].y
                                self.towers_menu.append(upgradeTower)
                            
                            # draw new tower
                            #screen.blit(tower_new.image, (tower_new.x, tower_new.y))
                            return
                        # if player hasn't enough money, then player gets no tower
                        elif self.money_user < tower.value:
                            pygame.mixer.Sound.play(self.not_enough_money_sound)

                # if there is no tower, player gets one
                if not hasTower:
                    if (self.money_user >= tower.value):
                        self.money_user -= tower.value
                        
                        tower_images = copy.copy(tower.images)
                        for i in range(0, len(tower_images)):
                            tower_images[i] = pygame.transform.scale(tower_images[i], (self.scale_images[0], self.scale_images[1]))
                        tower_new = Tower(self.select[0] * self.scale_images[0], self.select[1] * self.scale_images[1], tower_images, tower.level, tower.distance, tower.damage, tower.speed, tower.sound, tower.value)
                        self.tower_list.append(tower_new)
                        screen.blit(tower_new.image, (tower_new.x, tower_new.y))
                        
                        # show upgrade in shop                            
                        upgradeImages = copy.copy(tower_new.images)
                        for i in range(0, len(upgradeImages)):
                            upgradeImages[i] = pygame.transform.scale(upgradeImages[i], (upgradeImages[i].get_width()*2, upgradeImages[i].get_height()*2))
                        upgradeTower = None
                        self.towers_menu = []
                        if tower_new.level < len(tower_new.images)-1:
                            upgradeTower = copy.copy(tower_new)
                            upgradeTower.upgrade()
                            upgradeTower.images = upgradeImages
                            upgradeTower.x = self.towers_menu_original[0].x
                            upgradeTower.y = self.towers_menu_original[0].y
                            self.towers_menu.append(upgradeTower)
                    # no money, no tower!
                    else:
                        pygame.mixer.Sound.play(self.not_enough_money_sound)

    # find the x and y of selected field    
    def findeSelectField(self, screen, waypoints, x, y):
        flag = True
        # check if not clicked on a waypoint
        if not x >= self.num_tiles_x:
            for t in waypoints:
                if t[0] == x and t[1] == y or x > self.num_tiles_x or y > self.num_tiles_y:
                    flag = False
    
            if flag:
                # if there already exists a selected field
                if not self.select == None:
                    # but it is not the same as now clicked on, return the new selected field
                    if not (x == self.select[0] and y == self.select[1]):
                        self.select = [x, y]
                        return (x,y)
                    else:
                        # else remove the select
                        self.select = None
                        return None
                else: 
                    # no selected field existed, so return the new
                     self.select = [x, y]
                     return (x,y)
            else: 
                # waypoint is no select
                return None
    
    # return font in special size
    def font_futura(self, size):
        return pygame.font.Font("fonts/futurahandwritten/futurahandwritten.ttf", size)
    