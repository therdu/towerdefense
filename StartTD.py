import pygame
from PygameButton import Button
from StartGame import Game
from cryptography.fernet import Fernet
from ImageButton import ImageButton

class Menue:
    def __init__(self, screen):
        super().__init__()
        # init font
        pygame.font.init()
        self.screen = screen
        self.width = self.screen.get_width()
        self.height = self.screen.get_height()
        
        self.image_background = pygame.image.load("images/Hintergrundbild_menue.png")
        self.image_background = pygame.transform.scale(self.image_background, (self.width, self.height))
        
        self.image_window = pygame.image.load("images/window/window_template.png")
        self.image_window = pygame.transform.scale(self.image_window, (self.width - self.width/14, self.height - self.height/14))
        self.image_window_headline = pygame.image.load("images/window/window_template_headline.png")
        self.image_window_headline = pygame.transform.scale(self.image_window_headline, (self.width - self.width/14, self.height - self.height/14))
        
        self.image_music = pygame.image.load("images/note.png")
        self.image_noMusic = pygame.image.load("images/note_silent.png")
        
        self.image_sound = pygame.image.load("images/speaker.png")
        self.image_noSound = pygame.image.load("images/speaker_silent.png")
        
        
        self.image_title = pygame.image.load("fonts/title.png")
        self.image_title = pygame.transform.scale(self.image_title, (self.width/1.7, 
                                                                     ((self.width/1.7)/self.image_title.get_width()) * self.image_title.get_height()))
        self.level = [0, 0]
        self.startGame = False
        
        self.muteMusic = 0.2
        self.muteSound = 0.2
        
        # create the Buttons
        font = self.font_futura(25)
        color_font = [(24, 104, 108), (255, 255, 255), (255, 255, 255)]
        color_buttons = [(145, 201, 225), (145, 201, 225), (84, 153, 183)]
        color_border = [(0, 0, 0), (255, 255, 255), (255, 255, 255)]
        
        self.button_start = Button(self.width/2-200, self.height/7 + self.image_title.get_height() + 25, 400, 50, font, "Spiel starten", color_font, color_buttons, color_border)
        self.button_credits = Button(self.width/2-200, self.height/7 + self.image_title.get_height() + 175, 400, 50, font, "Credits", color_font, color_buttons, color_border)
        self.button_leave = Button(self.width/2-200, self.height/7 + self.image_title.get_height() + 250, 400, 50, font, "Beenden", color_font, color_buttons, color_border)
        self.button_highscore = Button(self.width/2-200, self.height/7 + self.image_title.get_height() + 100, 400, 50, font, "Highscoreliste", color_font, color_buttons, color_border)
        self.button_music = ImageButton(self.width-100, self.height-100, 64, 64, [self.image_music, self.image_music, self.image_music])
        self.button_sound = ImageButton(self.width-100, self.height-200, 64, 64, [self.image_sound, self.image_sound, self.image_sound])
        
        self.button_music.muted = True
        self.button_sound.muted = True
        self.level = 1
        self.highscore_list = []
    
    
    def main(self):
        self.highscore_list = self.readHighscorelist()
        running = True
        FPS = 30
        fpsClock = pygame.time.Clock()
        
         # load and play music
        pygame.mixer.music.load("sound/menue_music.mp3")
        pygame.mixer.music.set_volume(self.muteMusic)
        pygame.mixer.music.play(-1)
        
        
        buttons = [self.button_start, self.button_credits, self.button_highscore, self.button_leave, self.button_music, self.button_sound]
        
        title_width = self.image_title.get_width()
        title_height = self.image_title.get_height()
        
        while running:
            self.screen.blit(self.image_background, (0, 0))
            self.screen.blit(self.image_title, (self.width/2 - self.image_title.get_width()/2, self.height/15))
        
             # get all current events
            events = pygame.event.get()
            for event in events:
                # if quit, quit
                if event.type == pygame.QUIT:
                    running = False
                    return False
                for button in buttons:
                    button.handle_event(event)
                
                if self.button_leave.clicked:
                    running = False
                    return False
                
                if self.button_start.clicked:
                    running = self.chooseLevel()
                    if self.startGame:
                        return True
                
                if self.button_highscore.clicked:
                    running = self.showHighscore()
                
                if self.button_credits.clicked:
                    running = self.show_credits()
                    if not running:
                        return False
                    continue
                
                if self.button_music.clicked:
                    if self.muteMusic == 0:
                        self.button_music.image = self.image_music
                        self.button_music.image_active = self.image_music
                        self.button_music.image_inactive = self.image_music
                        self.button_music.image_hover = self.image_music
                        self.muteMusic = 0.2
                        
                    else: 
                        self.button_music.image = self.image_noMusic
                        self.button_music.image_active = self.image_noMusic
                        self.button_music.image_inactive = self.image_noMusic
                        self.button_music.image_hover = self.image_noMusic
                        self.muteMusic = 0
                    
                    pygame.mixer.music.set_volume(self.muteMusic)
                
                if self.button_sound.clicked:
                    if self.muteSound == 0:
                        self.button_sound.image = self.image_sound
                        self.button_sound.image_active = self.image_sound
                        self.button_sound.image_inactive = self.image_sound
                        self.button_sound.image_hover = self.image_sound
                        self.muteSound = 0.2
                        
                    else: 
                        self.button_sound.image = self.image_noSound
                        self.button_sound.image_active = self.image_noSound
                        self.button_sound.image_inactive = self.image_noSound
                        self.button_sound.image_hover = self.image_noSound
                        self.muteSound = 0
                    
                    for button in buttons:               
                            button.setVolume(self.muteSound)
                        
            for button in buttons:
                button.draw(self.screen)

            # update and show changes
            pygame.display.update()
            fpsClock.tick(FPS)
     
    def showHighscore(self):
        running = True
        fpsClock = pygame.time.Clock()
        
        label_title = self.font_futura(50).render("Top Ten", True, (255, 255, 255))
        title_rect = label_title.get_rect()
        title_rect.center = (self.width/2, self.height/28+60)
        
        # create the Buttons
        font = self.font_futura(25)
        color_font = [(126, 99, 50), (255, 255, 255), (255, 255, 255)]
        color_buttons = [(204, 159, 80), (204, 159, 80), (156, 122, 62)]
        color_border = [(0, 0, 0), (255, 255, 255), (255, 255, 255)]
             
        button_back = Button(3*self.width/6-100, 13*self.height/14 - 50, 200, 50, font, "Zurück", color_font, color_buttons, color_border)
             
        buttons = [button_back]
        
        for button in buttons:               
            button.setVolume(self.muteSound)
        
        highscore_list = self.readHighscorelist()
        
        while running:
            pygame.event.pump()
            self.screen.blit(self.image_background, (0, 0))
            self.screen.blit(self.image_window, (self.width/28, self.height/28))
            self.screen.blit(self.image_window_headline, (self.width/28, self.height/28))
            self.screen.blit(label_title, title_rect)
            for i in range(0, len(highscore_list[0])):
                label_name = self.font_futura(30).render(str(i+1)+". "+highscore_list[0][i], True, (255, 255, 255))
                nameRect = label_name.get_rect()
                nameRect.bottomleft = (self.width/2-300, self.height/28 + 250 + i*40)
                label_score = self.font_futura(30).render(highscore_list[1][i], True, (255, 255, 255))
                scoreRect = label_score.get_rect()
                scoreRect.bottomleft = (self.width/2+200, self.height/28 + 250 + i*40)
                self.screen.blit(label_name, nameRect)
                self.screen.blit(label_score, scoreRect)
            
             # get all current events
            events = pygame.event.get()
            for event in events:
                # if quit, quit
                if event.type == pygame.QUIT:
                    running = False
                    return False
                
                for button in buttons:
                    button.handle_event(event)
                
                if button_back.clicked:
                    running = False
                    return True
                
            for button in buttons:
                button.draw(self.screen)

            # update and show changes
            pygame.display.update()
            fpsClock.tick(30)
         
            
    # read and decrypt the Highscorelist
    def readHighscorelist(self):
          
        # opening the encrypted file
        with open('Highscore.txt', 'rb') as enc_file:
            encrypted = enc_file.read()
          
        # key
        key = b"1w2JFZKnG68Prrnblx_kbDJq4jTQ-6zpHbapKzzxv84="       
        fernet = Fernet(key)
        
        # decrypt
        highscoreRaw = fernet.decrypt(encrypted).decode('unicode_escape')
        
        # from string to array
        highscore_split = highscoreRaw.split("\n")
        
        for i in range(0, len(highscore_split)):
            highscore_split[i] = highscore_split[i].rsplit("\r", 1)[0]
            
        self.level = int(highscoreRaw[len(highscoreRaw)-1])
            
        highscore_names = highscore_split[0:10]
        highscore_points = highscore_split[11:21]
        highscore_list = [highscore_names, highscore_points]

        enc_file.close()
        return highscore_list
        
    def chooseLevel(self):
        running = True
        fpsClock = pygame.time.Clock()
        
        label_title = self.font_futura(50).render("Levelauswahl", True, (255, 255, 255))
        title_rect = label_title.get_rect()
        title_rect.center = (self.width/2, self.height/28+60)
        # create the Buttons
        font = self.font_futura(25)
        color_font = [(126, 99, 50), (255, 255, 255), (255, 255, 255)]
        color_buttons = [(204, 159, 80), (204, 159, 80), (156, 122, 62)]
        color_border = [(0, 0, 0), (255, 255, 255), (255, 255, 255)]
             
        button_start = Button(2*self.width/6-100, 13*self.height/15 - 50, 200, 50, font, "Level starten", color_font, color_buttons, color_border)
        button_endless = Button(3*self.width/6-100, 13*self.height/15 - 50, 200, 50, font, "Endlos starten", color_font, color_buttons, color_border)
        button_back = Button(4*self.width/6-100, 13*self.height/15 - 50, 200, 50, font, "Zurück", color_font, color_buttons, color_border)
        
        level_1 = Button(8*self.width/30, 4*self.height/15, 100, 100, font, "1", color_font, color_buttons, color_border)
        level_2 = Button(11*self.width/30, 4*self.height/15, 100, 100, font, "2", color_font, color_buttons, color_border)
        level_3 = Button(14*self.width/30, 4*self.height/15, 100, 100, font, "3", color_font, color_buttons, color_border)
        level_4 = Button(17*self.width/30, 4*self.height/15, 100, 100, font, "4", color_font, color_buttons, color_border)
        level_5 = Button(20*self.width/30, 4*self.height/15, 100, 100, font, "5", color_font, color_buttons, color_border)
        
        level_6 = Button(8*self.width/30, 6.5 * self.height / 15, 100, 100, font, "6", color_font, color_buttons, color_border)
        level_7 = Button(11*self.width/30, 6.5 * self.height / 15, 100, 100, font, "7", color_font, color_buttons, color_border)
        level_8 = Button(14*self.width/30, 6.5 * self.height / 15, 100, 100, font, "8", color_font, color_buttons, color_border)
        level_9 = Button(17*self.width/30, 6.5 * self.height / 15, 100, 100, font, "9", color_font, color_buttons, color_border)
        level_10 = Button(20*self.width/30, 6.5 * self.height / 15, 100, 100, font, "10", color_font, color_buttons, color_border)

        level_11 = Button(8 * self.width / 30, 9 * self.height / 15, 100, 100, font, "11", color_font, color_buttons,color_border)
        level_12 = Button(11 * self.width / 30, 9 * self.height / 15, 100, 100, font, "12", color_font,color_buttons, color_border)
        level_13 = Button(14 * self.width / 30, 9 * self.height / 15, 100, 100, font, "13", color_font,color_buttons, color_border)
        level_14 = Button(17 * self.width / 30, 9 * self.height / 15, 100, 100, font, "14", color_font,color_buttons, color_border)
        level_15 = Button(20 * self.width / 30, 9 * self.height / 15, 100, 100, font, "15", color_font,color_buttons, color_border)

        level_1.muted = True
        level_2.muted = True
        level_3.muted = True
        level_4.muted = True
        level_5.muted = True
        level_6.muted = True
        level_7.muted = True
        level_8.muted = True
        level_9.muted = True
        level_10.muted = True
        level_11.muted = True
        level_12.muted = True
        level_13.muted = True
        level_14.muted = True
        level_15.muted = True
        
        buttons = [button_start, button_endless, button_back]
        
        level = [level_1, level_2, level_3, level_4, level_5, level_6, level_7, level_8, level_9, level_10, level_11, level_12, level_13, level_14, level_15]
        # gesetzt um Level zu testen
        # self.level = 10
        for i in range(0, len(level)):
            if i > self.level:
                level[i].disabled = True
                level[i].handle_event(None)
        
        for button in buttons:               
            button.setVolume(self.muteSound)
        
        choosed_level = level_1
        choosed_level.color = choosed_level.color_active
        choosed_level.fontColor = choosed_level.fontColor_active
        level_1.choosed = True
        level_1.active = True
        if self.level == 0:
            button_endless.disabled = True
            button_endless.handle_event(None)
            
        while running:
            pygame.event.pump()
            self.screen.blit(self.image_background, (0, 0))
            self.screen.blit(self.image_window, (self.width/28, self.height/28))
            self.screen.blit(self.image_window_headline, (self.width/28, self.height/28))
            self.screen.blit(label_title, title_rect)
            
             # get all current events
            events = pygame.event.get()
            for event in events:
                # if quit, quit
                if event.type == pygame.QUIT:
                    running = False
                    return False
                
                for button in buttons:
                    button.handle_event(event)
                
                for l in level:
                    l.handle_event(event)
                    if l.clicked:
                        if int(l.text) > self.level:
                            button_endless.disabled = True
                            button_endless.handle_event(None)
                        else:
                            button_endless.disabled = False
                        if choosed_level == l:
                            choosed_level = l
                            choosed_level.color = choosed_level.color_active
                            choosed_level.fontColor = choosed_level.fontColor_active
                            l.choosed = True
                        else: 
                            choosed_level.choosed = False
                            choosed_level.active = False
                            choosed_level.color = choosed_level.color_inactive
                            choosed_level.fontColor = choosed_level.fontColor_inactive
                            choosed_level.colorBorder = choosed_level.colorBorder_inactive
                            
                            choosed_level = l
                            choosed_level.color = choosed_level.color_active
                            choosed_level.fontColor = choosed_level.fontColor_active
                            l.choosed = True
                
                if button_back.clicked:
                    running = False
                    return True
                
                if button_start.clicked:
                    running = False
                    self.startGame = True
                    self.level = [int(choosed_level.text)-1, 0]
                    return False
                
                if button_endless.clicked:
                    running = False
                    self.startGame = True
                    self.level = [int(choosed_level.text)-1, 1]
                    return False
                
            for button in buttons:
                button.draw(self.screen)
            
            for l in level:
                l.draw(self.screen)

            # update and show changes
            pygame.display.update()
            fpsClock.tick(30)
        
    def show_credits(self):
        running = True      
        fpsClock = pygame.time.Clock()
        # create the Buttons
        font = self.font_futura(25)
        color_font = [(24, 104, 108), (255, 255, 255), (255, 255, 255)]
        color_buttons = [(145, 201, 225), (145, 201, 225), (84, 153, 183)]
        color_border = [(0, 0, 0), (255, 255, 255), (255, 255, 255)]
        
        button_back = Button(self.width/2-200, 9*self.height/10 - 50, 400, 50, font, "Zurück", color_font, color_buttons, color_border)
        
        button_back.setVolume(self.muteSound)
        
        while running:
            pygame.event.pump()
            self.screen.blit(self.image_background, (0, 0))
            creditsText = ["Tower Defense Spiel - erstellt mit Pygame", "von Theresa Dünnebeil, Constanze Rudwaleit und Lucy Böttcher", " ", "sämtliche Grafiken sind selbst angefertigt", "Schriftarten von https://www.dafont.com/","Sound von https://www.zapsplat.com/", " ", "März 2022"]
            self.write(creditsText, 40, self.width/2, self.height/3)

             # get all current events
            events = pygame.event.get()
            for event in events:
                # if quit, quit
                if event.type == pygame.QUIT:
                    running = False
                    return False
                
                button_back.handle_event(event)
                
                if button_back.clicked:
                    running = False
                    return True

            button_back.draw(self.screen)

            # update and show changes
            pygame.display.update()
            fpsClock.tick(30)
    
    def write(self, text, distance, x, y):
        for i in range (0, len(text)):
            label = self.font_futura(20).render(text[i], True, (255, 255, 255))
            labelRect = label.get_rect()
            labelRect.center = (x, y+i*distance)
            self.screen.blit(label, labelRect)
        
    
     # return font in special size
    def font_futura(self, size):
        return pygame.font.Font("fonts/futurahandwritten/futurahandwritten.ttf", size)       

class TowerDefense:
    def __init__(self):
        super().__init__()   
        pygame.init()

        # size of field
        self.tile_y_scale = 1000
        self.tile_x_scale = 1000
        
        # window size
        self.x_scale = self.tile_x_scale+210/640*self.tile_y_scale
        
        # num of tiles
        self.num_tiles_x = 10
        self.num_tiles_y = 10
        
        # scale values
        self.scale_images = (self.tile_x_scale/self.num_tiles_x, self.tile_y_scale/self.num_tiles_y)
       
        # window images
        self.image_win_temp = pygame.image.load("images/window/window_template.png")
        self.image_win_temp = pygame.transform.scale(self.image_win_temp, (1.0*self.image_win_temp.get_width(), 1.3*self.image_win_temp.get_height()))
        self.image_win_temp_hl = pygame.image.load("images/window/window_template_headline.png")
        self.image_win_temp_hl = pygame.transform.scale(self.image_win_temp_hl, (1.0*self.image_win_temp_hl.get_width(), 1.3*self.image_win_temp_hl.get_height()))
        self.image_button = pygame.image.load("images/window/button.png")
        self.image_button_click = pygame.image.load("images/window/button_click.png")
        self.image_button_hover = pygame.image.load("images/window/button_hover.png")
        
        # icon
        pygame.display.set_icon(pygame.image.load("images/tower/tower1_upgrade_free.png"))
 
        # title
        self.title = 'Tower Defense Spiel'
        
        # init font
        pygame.font.init()
        
        self.status = 0 # 0 = Mainmenu, 1 = Game
        
        self.main()
        
    def main(self):
        
        running = True   
        
        # create screen
        screen = pygame.display.set_mode((self.x_scale, self.tile_y_scale))
        pygame.display.set_caption(self.title)
        
        mainMenu = Menue(screen)
        startGame = Game(screen, self.tile_y_scale, self.tile_x_scale, self.num_tiles_x, self.num_tiles_y)
                
        # main loop
        while running:
            pygame.event.pump()
            if self.status == 0:
                running = mainMenu.main()
                if mainMenu.startGame:
                    self.status = 1
                    mainMenu.startGame = False
                    continue
                else:
                    del startGame
                    del mainMenu   
                
            if self.status == 1:
                running = startGame.main(mainMenu.level, mainMenu.muteMusic, mainMenu.muteSound)
                self.status = 0
                continue
                
            
            # get all current events
            events = pygame.event.get()
            for event in events:
                # if quit, quit
                if event.type == pygame.QUIT:
                    running = False
                    break
                
                
           
        pygame.quit()

if __name__ == "__main__":
    game = TowerDefense()
