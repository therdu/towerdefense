import pygame

class Tower:

    def __init__(self, x, y, images, level, distance, damage, speed, sound, value):
        self.x = x
        self.y = y
        self.image = images[0]
        self.images = images
        self.level = level
        self.distance = distance
        self.damage = damage
        self.speed = speed
        self.cooldown = 0
        self.sound = sound
        self.value = value
        
        pygame.mixer.init()

    def upgrade(self):
        self.distance *= 1.2
        self.damage *= 1.5
        self.speed = round(2*self.speed/3)
        self.level += 1
        self.image = self.images[self.level]
        self.value *= 2

    def isInRange(self, pos):
        if (abs(pos[0]-self.x)**2 + abs(pos[1]-self.y)**2)**(1/2) <= self.distance:
            return True
        return False
    
    def checkForEnemy(self, enemies):
        if self.cooldown == 0:
            enemiesInRange = []
            for enemy in enemies:
                if self.isInRange(enemy.waypoints[enemy.pointerPosition]):
                    enemiesInRange.append(enemy)
            if len(enemiesInRange) > 0:
                firstEnemy = enemiesInRange[0]
                for enemy in enemiesInRange:
                    if enemy.pointerPosition/enemy.steps > firstEnemy.pointerPosition/firstEnemy.steps:
                        firstEnemy = enemy
                    
                pygame.mixer.Sound.play(self.sound)
                firstEnemy.getsHit(self.damage) 
                self.cooldown = self.speed
                   
        else:
            self.cooldown -= 1
                
            

            